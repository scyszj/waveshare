#include "stm32f0xx.h"
#include <stdio.h>
#include "usart.h"
#include "RTC_Time.h"

static volatile uint32_t TimingDelay;
void delay_1ms(uint32_t nTime);

int main(void)
{
    USART_Configuration();
    //NVIC_USART_Config();
    RTC_int();
    while (1)
    {
        RTC_TimeShow();
        RTC_AlarmShow();
        delay_1ms(1000);

    }
}
void delay_1ms(uint32_t nTime)
{

    SysTick_Config((uint32_t)6000);
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);
    TimingDelay = nTime;
    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}


