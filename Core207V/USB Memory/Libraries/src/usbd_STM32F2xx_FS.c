/*----------------------------------------------------------------------------
 *      RL-ARM - USB
 *----------------------------------------------------------------------------
 *      Name:    usbd_STM32F2xx_FS.c
 *      Purpose: Hardware Layer module for ST STM32F207ZE
 *      Rev.:    V4.21
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2011 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include <RTL.h>
#include <rl_usb.h>
#include <..\..\RL\USB\INC\usb.h>
#include <stm32f2xx.h>

#define __NO_USB_LIB_C
#include "usb_config_FS.c"

#define RX_FIFO_SIZE        128
#define TX0_FIFO_SIZE       64
#define TX1_FIFO_SIZE       64
#define TX2_FIFO_SIZE       64
#define TX3_FIFO_SIZE       64

#define TX_FIFO(n)          *((__packed U32*)(OTG_FS_BASE + 0x1000 + n*0x1000))
#define RX_FIFO             *((U32*)(OTG_FS_BASE + 0x1000))

#define DIEPTSIZ(EPNum)     *(&OTG_FS->DIEPTSIZ0 + EPNum * 8)
#define DIEPCTL(EPNum)      *(&OTG_FS->DIEPCTL0  + EPNum * 8)
#define DTXFSTS(EPNum)      *(&OTG_FS->DTXFSTS0  + EPNum * 8)
#define DTXFSTS(EPNum)      *(&OTG_FS->DTXFSTS0  + EPNum * 8)
#define DOEPTSIZ(EPNum)     *(&OTG_FS->DOEPTSIZ0 + EPNum * 8)
#define DOEPCTL(EPNum)      *(&OTG_FS->DOEPCTL0  + EPNum * 8)
#define DIEPINT(EPNum)      *(&OTG_FS->DIEPINT0  + EPNum * 8)
#define DOEPINT(EPNum)      *(&OTG_FS->DOEPINT0  + EPNum * 8)

U32 OutMaxPacketSize[4] = {USBD_MAX_PACKET0,0,0,0};
U32 TxFifoSize[4] = {TX0_FIFO_SIZE, TX1_FIFO_SIZE, TX2_FIFO_SIZE, TX3_FIFO_SIZE};

/*
 *  usbd_stm32_delay
 *    Parameters:      delay:      Delay
 *    Return Value:    None
 */
void usbd_stm32_delay (U32 delay) {
  delay *= SystemCoreClock / 100000;
  while (delay--) {
    __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop();
  }
}


/*
 *  USB Device Interrupt enable
 *   Called by USBD_Init to enable the USB Interrupt
 *    Return Value:    None
 */

#ifdef __RTX
void __svc(1) USBD_IntrEna (void);
void __SVC_1               (void) {
#else
void          USBD_IntrEna (void) {
#endif
  NVIC_EnableIRQ   (OTG_FS_IRQn);       /* Enable OTG interrupt               */
}


/*
 *  USB Device Initialize Function
 *   Called by the User to initialize USB
 *   Return Value:    None
 */

void USBD_Init (void) {

/* Configure  PA10, PA11, PA12 as alternate OTG_FS; PH4 as output             */
  RCC->AHB1ENR    |=  1;

  GPIOA->MODER    =   (GPIOA->MODER  & ~(3 << 20)) | (2  << 20);
  GPIOA->OTYPER  &=  ~(1 << 10);
  GPIOA->AFR[1]   =   (GPIOA->AFR[1] & ~(15 << 8)) | (10 <<  8);
  GPIOA->OSPEEDR |=   (3 << 20);
  GPIOA->PUPDR   &=  ~(3 << 20);

  GPIOA->MODER    =   (GPIOA->MODER  & ~(3  << 22)) | (2  << 22);
  GPIOA->OTYPER  &=  ~(1 << 11);
  GPIOA->AFR[1]   =   (GPIOA->AFR[1] & ~(15 << 12)) | (10 << 12);
  GPIOA->OSPEEDR |=   (3 << 22);
  GPIOA->PUPDR   &=  ~(3 << 22);

  GPIOA->MODER    =   (GPIOA->MODER  & ~(3  << 24)) | (2  << 24);
  GPIOA->OTYPER  &=  ~(1 << 12);
  GPIOA->AFR[1]   =   (GPIOA->AFR[1] & ~(15 << 16)) | (10 << 16);
  GPIOA->OSPEEDR |=   (3 << 24);
  GPIOA->PUPDR   &=  ~(3 << 24);

  RCC->AHB1ENR    |=  (1  <<  7);
  GPIOH->MODER     =  (GPIOH->MODER   & ~(3 << 10)) | (2  << 10);
  GPIOH->OTYPER  &=  ~(1 <<  5);
  GPIOH->OSPEEDR  =   (GPIOH->OSPEEDR & ~(3 << 10)) | (1  << 10);
  GPIOH->PUPDR   &=  ~(3 << 10);
  GPIOH->BSRRH   |=   (1 <<  5);

  RCC->AHB2ENR   |=  (1 <<  7);         /* OTG FS clock enable                */
  usbd_stm32_delay   (1000);
  RCC->AHB2RSTR  |=  (1 <<  7);         /* OTG FS reset                       */
  usbd_stm32_delay   (1000);            /* Wait ~10 ms                        */
  RCC->AHB2RSTR  &= ~(1 <<  7);         /* OTG FS not reset                   */
  usbd_stm32_delay   (4000);            /* Wait ~40 ms                        */

  OTG_FS->GUSBCFG  = (1 << 30);         /* force device mode                  */
  usbd_stm32_delay   (500);

  USBD_IntrEna();                       /* Enable OTG interrupt               */

  OTG_FS->GAHBCFG |= 1 | (1 << 7);
  OTG_FS->GUSBCFG  = (OTG_FS->GUSBCFG & ~(15UL << 10)) |(9 << 10);/*turnaround*/

  OTG_FS->DCFG    |=  3;                /* full speed phy                     */
  OTG_FS->PCGCCTL  =  0;                /* Restart PHY clock                  */

  OTG_FS->GINTMSK  = (1UL << 11) |      /* suspend int unmask                 */
                     (1UL << 12) |      /* reset int unmask                   */
                     (1UL << 13) |      /* enumeration done int unmask        */
                     (1UL << 4 ) |      /* recive fifo non-empty int  unmask  */
                     (1UL << 18) |      /* IN EP int unmask                   */
                     (1UL << 31) |      /* resume int unmask                  */
#ifdef __RTX
  ((USBD_RTX_DevTask   != 0) ? (1UL <<  3) : 0);   /* SOF int unmask          */
#else
  ((USBD_P_SOF_Event   != 0) ? (1UL <<  3) : 0);   /* SOF int unmask          */
#endif
}




/*
 *  USB Device Connect Function
 *   Called by the User to Connect/Disconnect USB Device
 *    Parameters:      con:   Connect/Disconnect
 *    Return Value:    None
 */

void USBD_Connect (BOOL con) {  
  if (con) {
    OTG_FS->GCCFG   =   (1UL << 19) |   /* enable Vbus*                       */
                        (1UL << 16);    /* power down deactivated             */
    OTG_FS->DCTL   &=  ~(1UL << 1 );    /* soft disconnect disabled           */

  }
  else {
    OTG_FS->GCCFG  &= ~(1UL << 19);     /* disable Vbus*                      */
    OTG_FS->DCTL   |=  (1UL << 1 );     /* soft disconnect enabled            */
  }
}


/*
 *  USB Device Reset Function
 *   Called automatically on USB Device Reset
 *    Return Value:    None
 */

void USBD_Reset (void) {
  U32 i;

  for (i = 0; i < (USBD_EP_NUM + 1); i++) {
   if (DOEPCTL(i) & (1UL << 31))
     DOEPCTL(i) = (1UL << 30) | (1UL << 27); /* OUT EP disable, Set NAK       */
   if (DIEPCTL(i) & (1UL << 31))
     DIEPCTL(i) = (1UL << 30) | (1UL << 27); /* IN EP disable, Set NAK        */
  }

  USBD_SetAddress(0 , 1);

  OTG_FS->DAINTMSK = 1 | (1UL << 16);   /* unmask IN&OUT EP0 interruts        */

  OTG_FS->DOEPMSK  =  (1UL << 3) |      /* setup phase done                   */
                       1;               /*transfer complete                   */
  OTG_FS->DIEPMSK  =   1;               /*transfer completed                  */

  OTG_FS->GRXFSIZ  =   RX_FIFO_SIZE;
  OTG_FS->DIEPTXF0 =  (TX0_FIFO_SIZE << 16) | RX_FIFO_SIZE;

  OTG_FS->DIEPTXF1 = (RX_FIFO_SIZE + TX0_FIFO_SIZE) |
                     (TX1_FIFO_SIZE << 16);

  OTG_FS->DIEPTXF2 = (RX_FIFO_SIZE + TX0_FIFO_SIZE + TX1_FIFO_SIZE) |
                     (TX2_FIFO_SIZE << 16);

  OTG_FS->DIEPTXF3 = (RX_FIFO_SIZE + TX0_FIFO_SIZE+ TX1_FIFO_SIZE +TX2_FIFO_SIZE) |
                     (TX3_FIFO_SIZE << 16);

  OTG_FS->DOEPTSIZ0   =  (3UL << 29) |  /* setup count = 3                    */
                         (1UL << 19) |  /* packet count                       */
                          USBD_MAX_PACKET0;
}


/*
 *  USB Device Suspend Function
 *   Called automatically on USB Device Suspend
 *    Return Value:    None
 */

void USBD_Suspend (void) {
}


/*
 *  USB Device Resume Function
 *   Called automatically on USB Device Resume
 *    Return Value:    None
 */

void USBD_Resume (void) {
}


/*
 *  USB Device Remote Wakeup Function
 *   Called automatically on USB Device Remote Wakeup
 *    Return Value:    None
 */

void USBD_WakeUp (void) {
  OTG_FS->DCTL |= 1;                    /* remote wakeup signaling            */
  usbd_stm32_delay (50);                /* Wait ~5 ms                         */
  OTG_FS->DCTL &= ~1;
}


/*
 *  USB Device Remote Wakeup Configuration Function
 *    Parameters:      cfg:   Device Enable/Disable
 *    Return Value:    None
 */

void USBD_WakeUpCfg (BOOL cfg) {
  /* Not needed                                                               */
}


/*
 *  USB Device Set Address Function
 *    Parameters:      adr:   USB Device Address
 *    Return Value:    None
 */

void USBD_SetAddress (U32  adr, U32 setup) {
  if (setup) {
    OTG_FS->DCFG = (OTG_FS->DCFG & ~(0x7f << 4)) | (adr << 4);
  }
}


/*
 *  USB Device Configure Function
 *    Parameters:      cfg:   Device Configure/Deconfigure
 *    Return Value:    None
 */

void USBD_Configure (BOOL cfg) {
}


/*
 *  Configure USB Device Endpoint according to Descriptor
 *    Parameters:      pEPD:  Pointer to Device Endpoint Descriptor
 *    Return Value:    None
 */

void USBD_ConfigEP (USB_ENDPOINT_DESCRIPTOR *pEPD) {
  U32 num, val, type;

  num  = pEPD->bEndpointAddress & ~(0x80);
  val  = pEPD->wMaxPacketSize;
  type = pEPD->bmAttributes & USB_ENDPOINT_TYPE_MASK;

  if (pEPD->bEndpointAddress & USB_ENDPOINT_DIRECTION_MASK) {
    OTG_FS->DAINTMSK |= (1UL  << num);  /* unmask IN EP int                   */
    DIEPCTL(num)      = (num  <<  22) | /* fifo number                        */
                        (type <<  18) | /* ep type                            */
                         val;           /* max packet size                    */
    if (((DIEPCTL(num) >> 18) & 3) > 1) /* if interrupt or bulk EP            */
      DIEPCTL(num) |= (1 << 28);    
  }
  else {
    OutMaxPacketSize[num] = val;
    OTG_FS->DAINTMSK     |= (1UL  <<    num) << 16; /* unmask IN EP int       */
    DOEPCTL(num)          = (type <<     18) |      /* EP type                */
                            (val  &   0x7FF);       /* max packet size        */
    DOEPTSIZ(num)         = (1UL  <<     19) |      /* packet count = 1       */
                            (val  & 0x7FFFF);       /* transfer size          */
    if (((DOEPCTL(num) >> 18) & 3) > 1)             /* if int or bulk EP      */
      DOEPCTL(num)       |= (1 << 28);
  }
}


/*
 *  Set Direction for USB Device Control Endpoint
 *    Parameters:      dir:   Out (dir == 0), In (dir <> 0)
 *    Return Value:    None
 */

void USBD_DirCtrlEP (U32 dir) {
  /* Not needed                                                               */
}


/*
 *  Enable USB Device Endpoint
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_EnableEP (U32 EPNum) {
  if (EPNum & 0x80) {
    EPNum &= ~0x80;
    DIEPCTL(EPNum)    |= (1UL << 15) |  /* EP active                          */
                         (1UL << 27);   /* set EP NAK                         */
    if (DIEPCTL(EPNum)&  (1UL << 31))
      DIEPCTL(EPNum)  |= (1UL << 30);   /* disable EP                         */
  }
  else {
    DOEPCTL(EPNum)    |= (1UL << 15) |  /* EP active                          */
                         (1UL << 31) |  /* enable EP                          */
                         (1UL << 26);   /* clear EP NAK                       */
  }
}


/*
 *  Disable USB Endpoint
 *    Parameters:      EPNum: Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_DisableEP (U32 EPNum) {
  if (EPNum & 0x80) {
    EPNum &= ~0x80;
    if (DIEPCTL(EPNum) &   (1UL << 31))
      DIEPCTL(EPNum)   |=  (1UL << 30); /* disable EP                         */
    DIEPCTL(EPNum)     |=  (1UL << 27); /* set EP NAK                         */
    DIEPCTL(EPNum)     &= ~(1UL << 15); /* deactivate EP                      */
  }
  else {
    if (DOEPCTL(EPNum) &   (1UL << 31))
      DOEPCTL(EPNum)   |=  (1UL << 30); /* disable EP                         */
    DOEPCTL(EPNum)     |=  (1UL << 27); /* set EP NAK                         */
    DOEPCTL(EPNum)     &= ~(1UL << 15); /* deactivate EP                      */
  }
}


/*
 *  Reset USB Device Endpoint
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_ResetEP (U32 EPNum) {
  if (EPNum & 0x80) {
    EPNum &= ~0x80;
    if (DIEPCTL(EPNum) &  (1UL << 31))
      DIEPCTL(EPNum)   |= (1UL << 30);  /* disable EP                         */
    DIEPCTL(EPNum)     |= (1UL << 27);  /* set EP NAK                         */

    OTG_FS->GRSTCTL = (OTG_FS->GRSTCTL & ~(0x1f << 6)) | /* flush EP fifo     */
                      (EPNum << 6)| (1UL << 5);
    while (OTG_FS->GRSTCTL & (1UL << 5));
  }
}


/*
 *  Set Stall for USB Device Endpoint
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_SetStallEP (U32 EPNum) {
  if (!(EPNum & 0x80)) {
    DOEPCTL(EPNum) |= (1 << 21);        /*set stall                           */
  }
  else {
    EPNum &= ~0x80;
    if (DIEPCTL(EPNum) &  (1UL << 31)) {
      DIEPCTL(EPNum)   |= (1UL << 30);
    }
    DIEPCTL(EPNum)     |= (1UL << 21);  /*set stall                           */

    OTG_FS->GRSTCTL     = (OTG_FS->GRSTCTL & ~(0x1f << 6)) | /* flush EP fifo */
                          (EPNum << 6)| (1UL << 5);

    while (OTG_FS->GRSTCTL & (1UL << 5));
  }
}


/*
 *  Clear Stall for USB Device Endpoint
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_ClrStallEP (U32 EPNum) {
  if (!(EPNum & 0x80)) {
    if (((DOEPCTL(EPNum) >> 18) & 3) > 1)/* if interrupt or bulk EP           */
      DOEPCTL(EPNum)    =  (DOEPCTL(EPNum) & ~(1 << 21)) |  (1 << 28);
    else
      DOEPCTL(EPNum)   &= ~(1UL << 21); /* clear stall                        */
  }
  else {
    EPNum &= ~0x80;
    if (DIEPCTL(EPNum) &  (1UL << 31))
      DIEPCTL(EPNum)   |= (1UL << 30);
    DIEPCTL(EPNum)     |= (1UL << 27);

    OTG_FS->GRSTCTL     = (OTG_FS->GRSTCTL & ~(0x1f << 6)) | /* flush EP fifo */
                          (EPNum << 6)| (1UL << 5);
    while (OTG_FS->GRSTCTL & (1UL << 5));

    if (((DIEPCTL(EPNum) >> 18) & 3) > 1)/* if interrupt or bulk EP           */
      DIEPCTL(EPNum)    =   (DIEPCTL(EPNum) & ~(1 << 21)) |  (1 << 28);
    else
      DIEPCTL(EPNum)   &= ~(1UL << 21); /* clear stall                        */
  }
}


/*
 *  Clear USB Device Endpoint Buffer
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_ClearEPBuf (U32 EPNum) {
  if (EPNum & 0x80) {
    EPNum &= ~0x80;
    OTG_FS->GRSTCTL = (OTG_FS->GRSTCTL & ~(0x1f << 6)) | /* flush EP fifo     */
                      (EPNum << 6)| (1UL << 5);
    while (OTG_FS->GRSTCTL & (1UL << 5));
  }
}


/*
 *  Read USB Device Endpoint Data
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *                     pData: Pointer to Data Buffer
 *    Return Value:    Number of bytes read
 */

U32 USBD_ReadEP (U32 EPNum, U8 *pData) {
  U32 i, sz;


  sz = ( OTG_FS->GRXSTSP >> 4) & 0x7FF;

  for (i = 0; i < (U32)((sz+3)/4); i++) {
    *((__packed U32 *)pData) = RX_FIFO;
    pData += 4;
  }

  DOEPTSIZ(EPNum)  =  (1UL << 19) |                  /* packet count          */
                      (OutMaxPacketSize[EPNum]    ); /* transfer size         */
  if (EPNum == 0) {
    DOEPTSIZ(0)   |= (3UL << 29);
  }
  DOEPCTL(EPNum)  |= (1UL <<31) | (1UL << 26);       /* clear NAK, enable EP  */
  OTG_FS->GINTMSK |= (1UL << 4);
  return (sz);
}


/*
 *  Write USB Device Endpoint Data
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *                     pData: Pointer to Data Buffer
 *                     cnt:   Number of bytes to write
 *    Return Value:    Number of bytes written
 */

U32 USBD_WriteEP (U32 EPNum, U8 *pData, U32 cnt) {
  U32 i;

  EPNum &= ~(0x80);

  if (cnt) {
    while (DTXFSTS(EPNum) < cnt);

    DIEPTSIZ(EPNum) = (DIEPTSIZ(EPNum) & ~0x1FFFFFFF) | cnt | (1UL << 19);
    DIEPCTL(EPNum) |= (1UL << 31) | (1UL << 26);
    for (i = 0; i < (cnt+3)/4; i++) {
      TX_FIFO(EPNum) = *((__packed U32 *)pData);
      pData +=4;
    }
  }
  else {
    DIEPTSIZ(EPNum) = (DIEPTSIZ(EPNum) & ~0x1FFFFFFF)| (1<< 19);
    DIEPCTL(EPNum) |= (1UL << 31) | (1UL << 26);
  }
  return (cnt);
}


/*
 *  Get USB Device Last Frame Number
 *    Parameters:      None
 *    Return Value:    Frame Number
 */

U32 USBD_GetFrame (void) {
  return ((OTG_FS->DSTS >> 8) & 0x3FFF);
}


/*
 *  USB Device Interrupt Service Routine
 */
void OTG_FS_IRQHandler(void) {
  U32 istr, val, num, i;

  istr = OTG_FS->GINTSTS & OTG_FS->GINTMSK;

/* reset interrupt                                                            */
  if (istr & (1UL << 12)) {
    USBD_Reset();
    usbd_reset_core();
#ifdef __RTX
    if (USBD_RTX_DevTask) {
      isr_evt_set(USBD_EVT_RESET, USBD_RTX_DevTask);
    }
#else
    if (USBD_P_Reset_Event) {
      USBD_P_Reset_Event();
    }
#endif
    OTG_FS->GINTSTS |= (1UL << 12);
  }

/* suspend interrupt                                                          */
  if (istr & (1UL << 11)) {
    USBD_Suspend();
#ifdef __RTX
    if (USBD_RTX_DevTask) {
      isr_evt_set(USBD_EVT_SUSPEND, USBD_RTX_DevTask);
    }
#else
    if (USBD_P_Suspend_Event) {
      USBD_P_Suspend_Event();
    }
#endif
    OTG_FS->GINTSTS |= (1UL << 11);
  }

/* resume interrupt                                                           */
  if (istr & (1UL << 31)) {
    USBD_Resume();
#ifdef __RTX
    if (USBD_RTX_DevTask) {
      isr_evt_set(USBD_EVT_RESUME, USBD_RTX_DevTask);
    }
#else
    if (USBD_P_Resume_Event) {
      USBD_P_Resume_Event();
    }
#endif
    OTG_FS->GINTSTS |= (1UL << 31);
  }

/* speed enumeration completed                                                */
  if (istr & (1UL << 13)) {
    OTG_FS->DIEPCTL0 &= ~(3UL);
    switch (USBD_MAX_PACKET0) {
      case 8:
        OTG_FS->DIEPCTL0 |= 3;
      break;

      case 16:
        OTG_FS->DIEPCTL0 |= 2;
      break;

      case 32:
        OTG_FS->DIEPCTL0 |= 1;
      break;
    }
    OTG_FS->DCTL |= (1 << 8);           /* clear global IN NAK                */
    OTG_FS->DCTL |= (1 << 10);          /* clear global OUT NAK               */
    OTG_FS->GINTSTS |= (1 << 13);
  }

/* Start Of Frame                                                             */
  if (istr & (1UL << 3)) {
#ifdef __RTX
    if (USBD_RTX_DevTask) {
      isr_evt_set(USBD_EVT_SOF, USBD_RTX_DevTask);
    }
#else
    if (USBD_P_SOF_Event) {
      USBD_P_SOF_Event();
    }
#endif
     OTG_FS->GINTSTS |= (1UL << 3);
  }

/* RxFIFO non-empty                                                           */
  if (istr & (1UL << 4)) {
    val = OTG_FS->GRXSTSR;
    num = val & 0x0F;
/* setup packet                                                               */
    if (((val >> 17) & 0x0F) == 6) {
      OTG_FS->GINTMSK &= ~(1UL << 4);
#ifdef __RTX
      if (USBD_RTX_EPTask[num]) {
        isr_evt_set(USBD_EVT_SETUP, USBD_RTX_EPTask[num]);
      }
#else
      if (USBD_P_EP[num]) {
        USBD_P_EP[num](USBD_EVT_SETUP);
      }
#endif
    }
/* OUT packet                                                                 */
    else if (((val >> 17) & 0x0F) == 2) { /* data out packed received         */
      OTG_FS->GINTMSK &= ~(1UL << 4);
#ifdef __RTX
      if (USBD_RTX_EPTask[num]) {
        isr_evt_set(USBD_EVT_OUT, USBD_RTX_EPTask[num]);
      }
#else
      if (USBD_P_EP[num]) {
        USBD_P_EP[num](USBD_EVT_OUT);
      }
#endif
    }
    else {
      OTG_FS->GRXSTSP;
    }
  }

/* IN Packet                                                                  */
  if (istr & (1UL << 18)) {
    num = (OTG_FS->DAINT & OTG_FS->DAINTMSK & 0xFFFF);
    for (i = 0; i < (USBD_EP_NUM+1); i++) {
      if ((num >> i) & 1) {
        num = i;
        break;
      }
    }
    if (DIEPINT(num) & 1) {             /* TxFIFO compleated                  */
#ifdef __RTX
      if (USBD_RTX_EPTask[num]) {
        isr_evt_set(USBD_EVT_IN,  USBD_RTX_EPTask[num]);
      }
#else
      if (USBD_P_EP[num]) {
        USBD_P_EP[num](USBD_EVT_IN);
      }
#endif
      DIEPINT(num) |= 1;
    }
  }
}
