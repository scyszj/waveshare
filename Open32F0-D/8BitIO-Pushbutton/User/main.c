#include "stm32f0xx.h"
#include <stdio.h>
#include "usart.h"

RCC_ClocksTypeDef RCC_Clocks;
static volatile uint32_t TimingDelay;
void Delay(uint32_t nTime);
void  Init_GPIOs(void);
int main(void)
{
    uint8_t Key;

    /* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500�s */
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

    Init_GPIOs();

    USART_Configuration();

    while (1)
    {
        if (!(GPIOC->IDR & 0x0001))
        {
            Key = 1;
        }
        else if (!(GPIOC->IDR & 0x0002))
        {
            Key = 2;
        }
        else if (!(GPIOA->IDR & 0x0010))
        {
            Key = 3;
        }
        else if (!(GPIOC->IDR & 0x0004))
        {
            Key = 4;
        }
        else if (!(GPIOC->IDR & 0x0008))
        {
            Key = 5;
        }
        else if (!(GPIOC->IDR & 0x2000))
        {
            Key = 6;
        }
        else if (!(GPIOC->IDR & 0x4000))
        {
            Key = 7;
        }
        else if (!(GPIOC->IDR & 0x8000))
        {
            Key = 8;
        }



        switch (Key)
        {
        case 1: printf("key0\r\n"); Delay(50); break;
        case 2: printf("key1\r\n"); Delay(50); break;
        case 3: printf("key2\r\n"); Delay(50); break;
        case 4: printf("key3\r\n"); Delay(50); break;
        case 5: printf("key4\r\n"); Delay(50); break;
        case 6: printf("key5\r\n"); Delay(50); break;
        case 7: printf("key6\r\n"); Delay(50); break;
        case 8: printf("key7\r\n"); Delay(50); break;
        }
        Key = 0;
    }

}

void  Init_GPIOs(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOA, ENABLE);

    /* Key */

    GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_0 | GPIO_Pin_1 |  GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);


}

void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}


