#include "stm32f0xx.h"
#include <stdio.h>
#include "usart.h"


static volatile uint32_t TimingDelay;
void Delay(uint32_t nTime);
int main(void)
{
    RCC_ClocksTypeDef RCC_Clocks;

    /* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500�s */
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

    USART_Configuration();
    NVIC_USART_Config();
    while (1)
    {
        printf("waveshare\r\n");
        //	printf("Receive:%d\r\n",USARTFLAG);
        Delay(100);
    }
}
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}


