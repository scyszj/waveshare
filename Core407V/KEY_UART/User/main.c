/**
 ******************************************************************************
 * @file    main.c
 * @author  MCD Application Team
 * @version V1.0.0
 * @date    18-April-2011
 * @brief   Main program body
 ******************************************************************************
 * @attention
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "usart.h"

/** @addtogroup STM32F2xx_StdPeriph_Examples
 * @{
 */

/** @addtogroup IOToggle
 * @{
 */

/* Private typedef -----------------------------------------------------------*/

void GPIO_Configuration(void);
void Delay(__IO uint32_t nCount);

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
int main(void)
{
    GPIO_Configuration();
    USART_Configuration();
    USART_NVIC_Config();
    printf("\r\nWelcome to WaveShare STM32F4 series MCU Board Port407V\r\n");
    printf("Please put the 8 push Buttons from 8 on IOs module, then press the button!!\r\n");
    while (1)
    {
        if (!(GPIOD->IDR & (1 << 0)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 0)))
            {
                printf("\r\nYou enter the key value:KY7\r\n");
                while (!(GPIOD->IDR & (1 << 0)))
                {
                    ;
                }
            }
        }
        else if (!(GPIOD->IDR & (1 << 1)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 1)))
            {
                printf("\r\nYou enter the key value:KY6\r\n");
                while (!(GPIOD->IDR & (1 << 1)))
                {
                    ;
                }
            }
        }
        else if (!(GPIOD->IDR & (1 << 2)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 2)))
            {
                printf("\r\nYou enter the key value:KY5\r\n");
                while (!(GPIOD->IDR & (1 << 2)))
                {
                    ;
                }
            }
        }
        else if (!(GPIOD->IDR & (1 << 3)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 3)))
            {
                printf("\r\nYou enter the key value:KY4\r\n");
                while (!(GPIOD->IDR & (1 << 3)))
                {
                    ;
                }
            }
        }

        else if (!(GPIOD->IDR & (1 << 4)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 4)))
            {
                printf("\r\nYou enter the key value:KY3\r\n");
                while (!(GPIOD->IDR & (1 << 4)))
                {
                    ;
                }
            }
        }

        else if (!(GPIOD->IDR & (1 << 5)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 5)))
            {
                printf("\r\nYou enter the key value:KY2\r\n");
                while (!(GPIOD->IDR & (1 << 5)))
                {
                    ;
                }
            }
        }
        else if (!(GPIOD->IDR & (1 << 6)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 6)))
            {
                printf("\r\nYou enter the key value:KY1\r\n");
                while (!(GPIOD->IDR & (1 << 6)))
                {
                    ;
                }
            }
        }
        else if (!(GPIOD->IDR & (1 << 7)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 7)))
            {
                printf("\r\nYou enter the key value:KY0\r\n");
                while (!(GPIOD->IDR & (1 << 7)))
                {
                    ;
                }
            }
        }

    }
}
void GPIO_Configuration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);

    //PD0 PD1 PD2 PD3 PD4 PD5 PD6 PD7
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOD, &GPIO_InitStructure);


}
/**
 * @brief  Delay Function.
 * @param  nCount:specifies the Delay time length.
 * @retval None
 */
void Delay(__IO uint32_t nCount)
{
    while (nCount--)
    {

    }
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {

    }
}
#endif

/**
 * @}
 */

/**
 * @}
 */

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
