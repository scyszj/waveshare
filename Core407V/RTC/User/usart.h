#ifndef _USART_H
#define _USART_H

#include <stdio.h>
#include "stm32f4xx.h"

extern void USART_Configuration(void);
extern void USART_NVIC_Config(void);

#endif /*_USART_H*/
