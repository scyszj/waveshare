#ifndef _18B20_H
#define _18B20_H

#include "stm32f0xx.h"
#include "OneWire.h"

void convertDs18b20(void);
uint8_t* readID(void);
uint8_t readTemp(void);

#endif /*_18B20_H*/

