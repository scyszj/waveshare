/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stdio.h>
/** @addtogroup STM32F30x_StdPeriph_Examples
 * @{
 */

/** @addtogroup I2C_EEPROM
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
typedef enum { FAILED = 0, PASSED = !FAILED } TestStatus;

/* Private define ------------------------------------------------------------*/
/* Uncomment the following line to enable using LCD screen for messages display */
#define ENABLE_LCD_MSG_DISPLAY

#define sEE_WRITE_ADDRESS1        0x00
#define sEE_READ_ADDRESS1         0x00
#define BUFFER_SIZE1             256
#define BUFFER_SIZE2             256
#define sEE_WRITE_ADDRESS2       (sEE_WRITE_ADDRESS1 + BUFFER_SIZE1)
#define sEE_READ_ADDRESS2        (sEE_READ_ADDRESS1 + BUFFER_SIZE1)

/* Private macro -------------------------------------------------------------*/
#define countof(a) (sizeof(a) / sizeof(*(a)))
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE * f)
#endif /* __GNUC__ */

/* Private variables ---------------------------------------------------------*/
uint8_t Tx1Buffer[256];

uint8_t Tx2Buffer[256];

uint8_t Rx1Buffer[256];

uint8_t Rx2Buffer[256];



__IO TestStatus TransferStatus1 = FAILED, TransferStatus2 = FAILED;
__IO uint32_t NumDataRead = 0;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
TestStatus Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint32_t BufferLength);
USART_InitTypeDef USART_InitStructure;
/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
int main(void)
{
    uint16_t i;

    /*!< At this stage the microcontroller clock setting is already configured,
       this is done through SystemInit() function which is called from startup
       file (startup_stm32f30x.s) before to branch to application main.
       To reconfigure the default setting of SystemInit() function, refer to
       system_stm32f30x.c file
     */
    /* USARTx configured as follow:
          - BaudRate = 115200 baud
          - Word Length = 8 Bits
          - One Stop Bit
          - No parity
          - Hardware flow control disabled (RTS and CTS signals)
          - Receive and transmit enabled
     */
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    STM_EVAL_COMInit(COM1, &USART_InitStructure);



    /* Add your application code here
     */
    for (i = 0; i < 256; i++)
    {
        Tx1Buffer[i] = i;  /* ���WriteBuffer */
    }
    for (i = 0; i < 256; i++)
    {
        Tx2Buffer[i] = i;  /* ���WriteBuffer */

    }
    /* Initialize the I2C EEPROM driver ----------------------------------------*/
    sEE_Init();

    /* First write in the memory followed by a read of the written data --------*/
    /* Write on I2C EEPROM from sEE_WRITE_ADDRESS1 */
    sEE_WriteBuffer(Tx1Buffer, sEE_WRITE_ADDRESS1, BUFFER_SIZE1);

    /* Wait for EEPROM standby state */
    sEE_WaitEepromStandbyState();

    /* Set the Number of data to be read */
    NumDataRead = BUFFER_SIZE1;

    /* Read from I2C EEPROM from sEE_READ_ADDRESS1 */
    sEE_ReadBuffer(Rx1Buffer, sEE_READ_ADDRESS1, (uint16_t*)(&NumDataRead));


    /* Check if the data written to the memory is read correctly */
    TransferStatus1 = Buffercmp(Tx1Buffer, Rx1Buffer, BUFFER_SIZE1);
    /* TransferStatus1 = PASSED, if the transmitted and received data
       to/from the EEPROM are the same */
    /* TransferStatus1 = FAILED, if the transmitted and received data
       to/from the EEPROM are different */
    if (TransferStatus1 == FAILED)
    {
        printf("\n\rBuffer1cmp FAILED\n\r");
    }
    else
    {
        printf("\n\rBuffer1cmp pass\n\r");
    }
    printf("aaaaaaaaaaaa");
    /* Second write in the memory followed by a read of the written data -------*/
    /* Write on I2C EEPROM from sEE_WRITE_ADDRESS2 */
    sEE_WriteBuffer(Tx2Buffer, sEE_WRITE_ADDRESS2, BUFFER_SIZE2);

    /* Wait for EEPROM standby state */
    sEE_WaitEepromStandbyState();

    /* Set the Number of data to be read */
    NumDataRead = BUFFER_SIZE2;

    /* Read from I2C EEPROM from sEE_READ_ADDRESS2 */
    sEE_ReadBuffer(Rx2Buffer, sEE_READ_ADDRESS2, (uint16_t*)(&NumDataRead));


    /* Check if the data written to the memory is read correctly */
    TransferStatus2 = Buffercmp(Tx2Buffer, Rx2Buffer, BUFFER_SIZE2);
    /* TransferStatus2 = PASSED, if the transmitted and received data
       to/from the EEPROM are the same */
    /* TransferStatus2 = FAILED, if the transmitted and received data
       to/from the EEPROM are different */
    if (TransferStatus2 == FAILED)
    {
        printf("\n\rBuffer2cmp FAILED\n\r");
    }
    else
    {
        printf("\n\rBuffer2cmp pass\n\r");
    }

    /* Free all used resources */
    sEE_DeInit();

    while (1)
    {
    }
}

#ifndef USE_DEFAULT_TIMEOUT_CALLBACK
/**
 * @brief  Example of timeout situation management.
 * @param  None.
 * @retval None.
 */
uint32_t sEE_TIMEOUT_UserCallback(void)
{
    /* Use application may try to recover the communication by resetting I2C
       peripheral (calling the function I2C_SoftwareResetCmd()) then re-start
       the transmission/reception from a previously stored recover point.
       For simplicity reasons, this example only shows a basic way for errors
       managements which consists of stopping all the process and requiring system
       reset. */


    /* Block communication and all processes */
    while (1)
    {
    }
}
#endif /* USE_DEFAULT_TIMEOUT_CALLBACK */

/**
 * @brief  Compares two buffers.
 * @param  pBuffer1, pBuffer2: buffers to be compared.
 * @param  BufferLength: buffer's length
 * @retval PASSED: pBuffer1 identical to pBuffer2
 *         FAILED: pBuffer1 differs from pBuffer2
 */
TestStatus Buffercmp(uint8_t* pBuffer1, uint8_t* pBuffer2, uint32_t BufferLength)
{
    while (BufferLength--)
    {
        if (*pBuffer1 != *pBuffer2)
        {
            return FAILED;
        }

        pBuffer1++;
        pBuffer2++;
    }

    return PASSED;
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(EVAL_COM1, (uint8_t)ch);

    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(EVAL_COM1, USART_FLAG_TC) == RESET)
    {
    }

    return ch;
}

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
