@echo off 
@set work_path=.
@cd %work_path% 
for /R %%s in (*.cpp,*.hpp,*.c,*.h) do ( 
    C:\uncrustify\Uncrustify.exe -c C:\uncrustify\linux.cfg --no-backup %%s
) 
pause 
