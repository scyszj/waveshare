#include "stm32f0xx.h"
#include <stdio.h>
#include "usart.h"
#include "ws_AT45DBXX.h"

RCC_ClocksTypeDef RCC_Clocks;
static volatile uint32_t TimingDelay;
void Delay(uint32_t nTime);

int main(void)
{
    unsigned char num = 0;
    unsigned char ID[4], data[255];

    /* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500�s */
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

    USART_Configuration();


    AT45DBXX_Init();
    printf("AT45DBXX had been Init!\r\n");
    AT45DBXX_Read_ID(ID);
    printf("AT45DBXX ID is");
    for (num = 0; num < 4; num++)
    {
        printf(" 0x%x ", ID[num]);
    }
    printf("\r\n");
    printf("\r\nWrite 255 byte data to buff1:\r\n");
    for (num = 0; num < 255; num++)
    {
        write_buffer(num, num);
        printf(" %d ", num);
    }
    printf("\r\nRead 255 byte data from buff1:\r\n");
    for (num = 0; num < 255; num++)
    {
        data[num] = read_buffer((unsigned int)num);
        printf(" %d ", data[num]);
    }

    while (1)
    {
        ;
    }

}
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}


