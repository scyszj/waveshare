/**
 ******************************************************************************
 * @file    usbh_usr.c
 * @author  MCD Application Team
 * @version V1.0.0
 * @date    11/29/2010
 * @brief   This file includes the usb host library user callbacks
 ******************************************************************************
 * @copy
 *
 * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
 * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
 * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
 * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
 * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
 * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 *
 * <h2><center>&copy; COPYRIGHT 2010 STMicroelectronics</center></h2>
 */

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "LCD.h"
#include "usbh_usr.h"
#include "ls.h"
#include "usbh_msc_core.h"
#include "usbh_msc_scsi.h"
#include "usbh_msc_bot.h"




/** @addtogroup USBH_USER
 * @{
 */

/** @addtogroup USBH_MSC_DEMO_USER_CALLBACKS
 * @{
 */

/** @defgroup USBH_USR
 * @brief    This file includes the usb host stack user callbacks
 * @{
 */

/** @defgroup USBH_USR_Private_TypesDefinitions
 * @{
 */
/**
 * @}
 */


/** @defgroup USBH_USR_Private_Defines
 * @{
 */
/**
 * @}
 */


/** @defgroup USBH_USR_Private_Macros
 * @{
 */
/**
 * @}
 */


/** @defgroup USBH_USR_Private_Variables
 * @{
 */
uint8_t USBH_USR_ApplicationState = USH_USR_FS_INIT;
uint8_t filenameString[15]  = { 0 };
EmbeddedFileSystem efs;
EmbeddedFile file;
EmbeddedFile fileR;
DirList list;
unsigned long ImgByteOffset;
uint8_t temp[]                        = "          \n";
uint8_t buffer[1024];

extern uint8_t Line;



/*  Points to the DEVICE_PROP structure of current device */
/*  The purpose of this register is to speed up the execution */

USBH_Usr_cb_TypeDef USR_Callbacks =
{
    USBH_USR_Init,
    USBH_USR_DeviceAttached,
    USBH_USR_ResetDevice,
    USBH_USR_DeviceDisconnected,
    USBH_USR_OverCurrentDetected,
    USBH_USR_DeviceSpeedDetected,
    USBH_USR_Device_DescAvailable,
    USBH_USR_DeviceAddressAssigned,
    USBH_USR_Configuration_DescAvailable,
    USBH_USR_Manufacturer_String,
    USBH_USR_Product_String,
    USBH_USR_SerialNum_String,
    USBH_USR_EnumerationDone,
    USBH_USR_UserInput,
    USBH_USR_DeviceNotSupported,
    USBH_USR_UnrecoveredError

};

typedef struct
{
    uint8_t pic_head[2];
    uint16_t pic_size_l;
    uint16_t pic_size_h;
    uint16_t pic_nc1;
    uint16_t pic_nc2;
    uint16_t pic_data_address_l;
    uint16_t pic_data_address_h;
    uint16_t pic_message_head_len_l;
    uint16_t pic_message_head_len_h;
    uint16_t pic_w_l;
    uint16_t pic_w_h;
    uint16_t pic_h_l;
    uint16_t pic_h_h;
    uint16_t pic_bit;
    uint16_t pic_dip;
    uint16_t pic_zip_l;
    uint16_t pic_zip_h;
    uint16_t pic_data_size_l;
    uint16_t pic_data_size_h;
    uint16_t pic_dipx_l;
    uint16_t pic_dipx_h;
    uint16_t pic_dipy_l;
    uint16_t pic_dipy_h;
    uint16_t pic_color_index_l;
    uint16_t pic_color_index_h;
    uint16_t pic_other_l;
    uint16_t pic_other_h;
    uint16_t pic_color_p01;
    uint16_t pic_color_p02;
    uint16_t pic_color_p03;
    uint16_t pic_color_p04;
    uint16_t pic_color_p05;
    uint16_t pic_color_p06;
    uint16_t pic_color_p07;
    uint16_t pic_color_p08;
}BMP_HEAD;

BMP_HEAD bmp;

typedef struct
{
    uint16_t x;                         /* LCD坐标X */
    uint16_t y;                         /* LCD坐标Y	*/
    uint8_t r;                          /* RED */
    uint8_t g;                          /* GREEN */
    uint8_t b;                          /* BLUE	*/
}BMP_POINT;

BMP_POINT point;

/**
 * @}
 */

/** @defgroup USBH_USR_Private_Constants
 * @{
 */
/*--------------- LCD Messages ---------------*/
const uint8_t MSG_HOST_INIT[]        = "> HOST LIBRARY INITIALIZED          ";
const uint8_t MSG_DEV_ATTACHED[]     = "> DEVICE ATTACHED                   ";
const uint8_t MSG_DEV_DISCONNECTED[] = "> DEVICE DISCONNECTED               ";
const uint8_t MSG_DEV_ENUMERATED[]   = "> ENUMERATION COMPLETED             ";
const uint8_t MSG_DEV_FULLSPEED[]    = "> DEVICE IS FULL SPEED              ";
const uint8_t MSG_DEV_LOWSPEED[]     = "> DEVICE IS LOW SPEED               ";
const uint8_t MSG_DEV_ERROR[]        = "> DEVICE ERROR                      ";

const uint8_t MSG_MSC_CLASS[]      = "> DEVICE SUPPORTS MASS STORAGE CLASS";
const uint8_t MSG_HID_CLASS[]      = "> DEVICE SUPPORTS HID CLASS";
const uint8_t MSG_DISK_SIZE[]      = "* Size of the disk in MBytes: ";
const uint8_t MSG_LUN[]            = "* LUN Available in the device:";
const uint8_t MSG_ROOT_CONT[]      = "* Root folder has the follow entries:";
const uint8_t MSG_WR_PROTECT[]      = "> The disk is write protected          ";
const uint8_t MSG_UNREC_ERROR[]     = "> UNRECOVERED ERROR STATE              ";

/**
 * @}
 */



/** @defgroup USBH_USR_Private_FunctionPrototypes
 * @{
 */
/**
 * @}
 */


/** @defgroup USBH_USR_Private_Functions
 * @{
 */


/**
 * @brief  USBH_USR_Init
 *         Displays the message on LCD for host lib initialization
 * @param  None
 * @retval None
 */
void USBH_USR_Init(void)
{
    /* Set default screen color*/
    LCD_Clear(Black);

    Line = 0;
    /* Display the application header */
    LCD_SetBackColor(Blue);
    LCD_SetTextColor(White);

    LCD_DisplayStringLine( Line++, "       STM32F105/7 USB HOST DEMO       ");

    printf("       STM32F105/7 USB HOST DEMO       \r\n");

    LCD_SetBackColor(Black);

}

/**
 * @brief  USBH_USR_DeviceAttached
 *         Displays the message on LCD on device attached
 * @param  None
 * @retval None
 */
void USBH_USR_DeviceAttached(void)
{
    LCD_DisplayStringLine( Line++, (uint8_t*)MSG_DEV_ATTACHED);
    printf("%s", MSG_DEV_ATTACHED);
    printf("\r\n");
}


/**
 * @brief  USBH_USR_UnrecoveredError
 * @param  None
 * @retval None
 */
void USBH_USR_UnrecoveredError(void)
{

    /* Set default screen color*/
    LCD_Clear(Black);

    Line = 0;

    /* Display the application header */
    LCD_SetBackColor(Blue);
    LCD_SetTextColor(White);

    LCD_DisplayStringLine( Line++, "       STM32F105/7 USB HOST DEMO       ");
    printf("       STM32F105/7 USB HOST DEMO       \r\n");

    LCD_SetBackColor(Red);

    LCD_DisplayStringLine( Line++,  (uint8_t*)MSG_UNREC_ERROR);
    printf("%s", MSG_UNREC_ERROR);
    printf("\r\n");

    LCD_SetBackColor(Black);
}


/**
 * @brief  USBH_DisconnectEvent
 *         Device disconnect event
 * @param  None
 * @retval Staus
 */
void USBH_USR_DeviceDisconnected(void)
{

    /* Set default screen color*/
    LCD_Clear(Black);

    Line = 0;

    /* Display the application header */
    LCD_SetBackColor(Blue);
    LCD_SetTextColor(White);

    LCD_DisplayStringLine( Line++, "       STM32F105/7 USB HOST DEMO       ");
    printf("       STM32F105/7 USB HOST DEMO       \r\n");

    LCD_SetBackColor(Black);

    LCD_DisplayStringLine( Line++, "> DEVICE DISCONNECTED!");
    printf("> DEVICE DISCONNECTED! \r\n");

}
/**
 * @brief  USBH_USR_ResetUSBDevice
 * @param  None
 * @retval None
 */
void USBH_USR_ResetDevice(void)
{
    /* callback for USB-Reset */
}


/**
 * @brief  USBH_USR_DeviceSpeedDetected
 *         Displays the message on LCD for device speed
 * @param  Device speed
 * @retval None
 */
void USBH_USR_DeviceSpeedDetected(uint8_t DeviceSpeed)
{
    if (DeviceSpeed == HPRT0_PRTSPD_FULL_SPEED)
    {
        LCD_DisplayStringLine( Line++, (uint8_t*)MSG_DEV_FULLSPEED);
        printf("%s", MSG_DEV_FULLSPEED);
        printf("\r\n");
    }
    else if (DeviceSpeed == HPRT0_PRTSPD_LOW_SPEED)
    {
        LCD_DisplayStringLine( Line++, (uint8_t*)MSG_DEV_LOWSPEED);
        printf("%s", MSG_DEV_LOWSPEED);
        printf("\r\n");
    }
    else
    {
        LCD_DisplayStringLine( Line++, (uint8_t*)MSG_DEV_ERROR);
        printf("%s", MSG_DEV_ERROR);
        printf("\r\n");
    }
}

/**
 * @brief  USBH_USR_Device_DescAvailable
 *         Displays the message on LCD for device descriptor
 * @param  device descriptor
 * @retval None
 */
void USBH_USR_Device_DescAvailable(void *DeviceDesc)
{
    uint8_t temp[50];
    USBH_DevDesc_TypeDef *hs;

    hs = DeviceDesc;


    sprintf((char*)temp, "VID          = %04Xh", (uint32_t)(*hs).idVendor);
    LCD_DisplayStringLine( Line++, temp);
    printf("VID          = %04Xh \r\n", (uint32_t)(*hs).idVendor);

    sprintf((char*)temp, "PID          = %04Xh", (uint32_t)(*hs).idProduct);
    LCD_DisplayStringLine( Line++, temp);
    printf("PID          = %04Xh \r\n", (uint32_t)(*hs).idProduct);

}

/**
 * @brief  USBH_USR_DeviceAddressAssigned
 *         USB device is successfully assigned the Address
 * @param  None
 * @retval None
 */
void USBH_USR_DeviceAddressAssigned(void)
{

}


/**
 * @brief  USBH_USR_Conf_Desc
 *         Displays the message on LCD for configuration descriptor
 * @param  Configuration descriptor
 * @retval None
 */
void USBH_USR_Configuration_DescAvailable(USBH_CfgDesc_TypeDef * cfgDesc,
                                          USBH_InterfaceDesc_TypeDef *itfDesc,
                                          USBH_EpDesc_TypeDef *epDesc)
{
    USBH_InterfaceDesc_TypeDef *id;
    uint8_t temp[50];

    id = itfDesc;

    sprintf((char*)temp, "NumEndpoints = %d", (uint32_t)(*id).bNumEndpoints);
    LCD_DisplayStringLine( Line++, temp);
    printf("NumEndpoints = %d \r\n", (uint32_t)(*id).bNumEndpoints);


    if ((*id).bInterfaceClass  == 0x08)
    {

        LCD_DisplayStringLine( Line++, (uint8_t*)MSG_MSC_CLASS);
        printf("%s", MSG_MSC_CLASS);
        printf("\r\n");

    }
    else if ((*id).bInterfaceClass  == 0x03)
    {

        LCD_DisplayStringLine( Line++, (uint8_t*)MSG_HID_CLASS);
        printf("%s", MSG_HID_CLASS);
        printf("\r\n");
    }
}

/**
 * @brief  USBH_USR_Manufacturer_String
 *         Displays the message on LCD for Manufacturer String
 * @param  Manufacturer String
 * @retval None
 */
void USBH_USR_Manufacturer_String(void *ManufacturerString)
{
    char temp[100];

    sprintf(temp, "Manufacturer : %s", (char*)ManufacturerString);
    LCD_DisplayStringLine( Line++, (uint8_t*)temp);
    printf("Manufacturer : %s \r\n", (char*)ManufacturerString);
}

/**
 * @brief  USBH_USR_Product_String
 *         Displays the message on LCD for Product String
 * @param  Product String
 * @retval None
 */
void USBH_USR_Product_String(void *ProductString)
{
    char temp[100];

    sprintf((char*)temp, "Product : %s", (char*)ProductString);
    LCD_DisplayStringLine( Line++, (uint8_t*)temp);
    printf("Product : %s \r\n", (char*)ProductString);
}

/**
 * @brief  USBH_USR_SerialNum_String
 *         Displays the message on LCD for SerialNum_String
 * @param  SerialNum_String
 * @retval None
 */
void USBH_USR_SerialNum_String(void *SerialNumString)
{
    uint8_t temp[100];

    sprintf((char*)temp, "Serial Number : %s", (char*)SerialNumString);
    LCD_DisplayStringLine( Line++, temp);
    printf("Serial Number : %s \r\n", (char*)SerialNumString);

}



/**
 * @brief  EnumerationDone
 *         User response request is displayed to ask application jump to class
 * @param  None
 * @retval None
 */
void USBH_USR_EnumerationDone(void)
{

    /* Enumeration complete */
    LCD_DisplayStringLine( Line++, (uint8_t*)MSG_DEV_ENUMERATED);
    printf("%s \r\n", MSG_DEV_ENUMERATED);


    /* Ask for user response to execute jump to application display */
    LCD_DisplayStringLine( Line++, \
                           "----------------------------------------");

    printf("----------------------------------------");
    printf("\r\n");


    LCD_DisplayStringLine( Line++, \
                           " To see the root content of the disk :  ");

    printf(" To see the root content of the disk :  ");
    printf("\r\n");

    LCD_DisplayStringLine( Line++, \
                           "'Press Key ...'                       ");

    printf("'Press Key ...'                       ");
    printf("\r\n");
}


/**
 * @brief  USBH_USR_DeviceNotSupported
 *         Device is not supported
 * @param  None
 * @retval None
 */
void USBH_USR_DeviceNotSupported(void)
{

    /* Set default screen color*/
    LCD_Clear(Black);

    Line = 0;

    /* Display the application header */
    LCD_SetBackColor(Blue);
    LCD_SetTextColor(White);

    LCD_DisplayStringLine( Line++, "       STM32F105/7 USB HOST DEMO       ");
    printf("       STM32F105/7 USB HOST DEMO       \r\n");


    LCD_SetBackColor(Black);
    LCD_DisplayStringLine( Line++, \
                           ">THE FIRMWARE SUPPORTS MSC CLASS DEVICES");

    printf(">THE FIRMWARE SUPPORTS MSC CLASS DEVICES  \r\n");

    LCD_DisplayStringLine( Line++, \
                           "  WITH SINGLE LUN                       ");

    printf("  WITH SINGLE LUN                       \r\n");


    LCD_DisplayStringLine( Line++, \
                           ">ATTACHED DEVICE IS NOT SUPPORTED!      ");

    printf(">ATTACHED DEVICE IS NOT SUPPORTED!      \r\n");


    if (MSC_Machine.maxLun > 0)
    {
        LCD_DisplayStringLine( Line++, "> Contains more than one LUN!");

        printf("> Contains more than one LUN! \r\n ");

        /*Display the LUN in the connected device*/
        USBH_USR_HexToASCII((uint32_t)(MSC_Machine.maxLun));

        LCD_SetBackColor(Blue);
        LCD_DisplayStringLine( Line++, (uint8_t*)MSG_LUN);

        printf("%s", MSG_LUN);
        printf("\r\n");


        LCD_SetBackColor(Black);
        LCD_DisplayStringLine( Line++, \
                               "-----------------------------------------");

        printf("-----------------------------------------");
        printf("\r\n");

        LCD_DisplayStringLine( Line++, temp);

        printf( "%s", temp );
        printf("\r\n");

        LCD_DisplayStringLine( Line++, \
                               "-----------------------------------------");

        printf("-----------------------------------------");
        printf("\r\n");
    }

    /* 2 seconds delay */
    USB_OTG_BSP_mDelay(2000);


}


/**
 * @brief  USBH_USR_UserInput
 *         User Action for application state entry
 * @param  None
 * @retval USBH_USR_Status : User response for key button
 */
USBH_USR_Status USBH_USR_UserInput(void)
{
    USBH_USR_Status usbh_usr_status;

    usbh_usr_status = USBH_USR_NO_RESP;

    /*Key is in polling mode to detect user action */
    if ( !GPIO_ReadInputDataBit(KEYA_PORT, KEYA_PORT_PIN) || GPIO_ReadInputDataBit(KEYB_PORT, KEYB_PORT_PIN) || \
         GPIO_ReadInputDataBit(KEYC_PORT, KEYC_PORT_PIN) )
    {

        usbh_usr_status = USBH_USR_RESP_OK;

    }
    return usbh_usr_status;
}

/**
 * @brief  USBH_USR_OverCurrentDetected
 *         Over Current Detected on VBUS
 * @param  None
 * @retval Staus
 */
void USBH_USR_OverCurrentDetected(void)
{

    /*Display color on LCD------------------------------------------------------*/
    LCD_Clear(Black);

    Line = 0;

    /* Display the application header */
    LCD_SetBackColor(Blue);
    LCD_SetTextColor(White);

    LCD_DisplayStringLine( Line++, "       STM32F105/7 USB HOST DEMO       ");

    printf("       STM32F105/7 USB HOST DEMO       \r\n");

    LCD_SetBackColor(Black);

    /*Display Title------------------------------------------------------------*/
    /* Set the LCD Text Color */
    LCD_SetTextColor(White);
    LCD_DisplayStringLine( Line++, "ERR : OVERCURRENT DETECTED ");

    printf("ERR : OVERCURRENT DETECTED \r\n ");


    /* 2 seconds delay */
    USB_OTG_BSP_mDelay(2000);
}


/**
 * @brief  USBH_USR_MSC_Application
 *         Demo application for mass storage
 * @param  None
 * @retval Staus
 */
int USBH_USR_MSC_Application(void)
{
    uint8_t bmpImageFound = FALSE;

    uint8_t textFileBuffer[] = "*   Thank you for using HY-GoldBull V3.0 Development Board ！^_^  *\r\n";

    switch (USBH_USR_ApplicationState)
    {
    case USH_USR_FS_INIT:

        /* Initialises the EFS lib*/
        if (efs_init(&efs, 0) != 0)
        {
            /* efs initialisation fails*/
            return -1;
        }

        /*Display the size of the disk*/
        USBH_USR_HexToASCII((((USBH_MSC_Param.MSCapacity) * \
                              (USBH_MSC_Param.MSPageLength)) / (1048576)));
        LCD_Clear(Black);
        Line = 0;

        LCD_SetBackColor(Blue);

        LCD_DisplayStringLine( Line++, (uint8_t*)MSG_DISK_SIZE);

        printf("%s", MSG_DISK_SIZE);
        printf("\r\n");

        LCD_SetBackColor(Black);
        LCD_DisplayStringLine( Line++, \
                               "-----------------------------------------");

        printf("-----------------------------------------");
        printf("\r\n");

        LCD_DisplayStringLine( Line++, temp);

        printf("%s", temp);
        printf("\r\n");

        LCD_DisplayStringLine( Line++, \
                               "-----------------------------------------");

        printf("-----------------------------------------");
        printf("\r\n");

        if (USBH_MSC_Param.MSWriteProtect == DISK_WRITE_PROTECTED)
        {
            LCD_DisplayStringLine( Line++, (uint8_t*)MSG_WR_PROTECT);

            printf("%s", MSG_WR_PROTECT);
            printf("\r\n");

            LCD_DisplayStringLine( Line++, \
                                   "-----------------------------------------");

            printf("-----------------------------------------");
            printf("\r\n");
        }


        USBH_USR_ApplicationState = USH_USR_FS_READLIST;
        break;

    case USH_USR_FS_READLIST:

        /*Reads the dis list*/
        if (ls_openDir(&list, &(efs.myFs), "/") != 0)
        {
            /*Could not open the selected directory*/
            return -2;
        }

        LCD_SetBackColor(Blue);
        LCD_DisplayStringLine( Line++, (uint8_t*)MSG_ROOT_CONT);

        printf("%s", MSG_ROOT_CONT);
        printf("\r\n");

        LCD_SetBackColor(Black);
        LCD_DisplayStringLine( Line++, \
                               "-----------------------------------------");

        printf("-----------------------------------------");
        printf("\r\n");


        while ((HCD_IsDeviceConnected(&USB_OTG_FS_dev)) && (ls_getNext(&list) == 0))
        {

            filenameString[0]  = list.currentEntry.FileName[0];
            filenameString[1]  = list.currentEntry.FileName[1];
            filenameString[2]  = list.currentEntry.FileName[2];
            filenameString[3]  = list.currentEntry.FileName[3];
            filenameString[4]  = list.currentEntry.FileName[4];
            filenameString[5]  = list.currentEntry.FileName[5];
            filenameString[6]  = list.currentEntry.FileName[6];
            filenameString[7]  = list.currentEntry.FileName[7];
            filenameString[8]  = '.';
            filenameString[9]  = list.currentEntry.FileName[8];
            filenameString[10] = list.currentEntry.FileName[9];
            filenameString[11] = list.currentEntry.FileName[10];

            LCD_DisplayStringLine( Line++, filenameString);

            printf("%s", filenameString);
            printf("\r\n");

            if ( Line == 13 )
            {
                LCD_DisplayStringLine( Line, "To continue 'Press Key' ");

                printf("To continue 'Press Key' ");
                printf("\r\n");

                /*Key in polling*/
                while ( !((HCD_IsDeviceConnected(&USB_OTG_FS_dev)) && \
                          ( !GPIO_ReadInputDataBit(KEYA_PORT, KEYA_PORT_PIN) || GPIO_ReadInputDataBit(KEYB_PORT, KEYB_PORT_PIN) || \
                            GPIO_ReadInputDataBit(KEYC_PORT, KEYC_PORT_PIN)) ) )
                {
                    ;
                }

                LCD_Clear(Black);
                Line = 0;
            }
        }

        LCD_DisplayStringLine( Line, "To continue 'Press Key' ");
        printf("To continue 'Press Key' ");
        printf("\r\n");

        /*Key in polling*/
        while ( !((HCD_IsDeviceConnected(&USB_OTG_FS_dev)) && \
                  ( !GPIO_ReadInputDataBit(KEYA_PORT, KEYA_PORT_PIN) || GPIO_ReadInputDataBit(KEYB_PORT, KEYB_PORT_PIN) || \
                    GPIO_ReadInputDataBit(KEYC_PORT, KEYC_PORT_PIN)) ) )
        {
            ;
        }

        LCD_Clear(Black);
        Line = 0;

        USBH_USR_ApplicationState = USH_USR_FS_WRITEFILE;
        break;

    case USH_USR_FS_WRITEFILE:

        /* Writes a text file, HOSTDEMO.TXT in the disk*/

        if (USBH_MSC_Param.MSWriteProtect == DISK_WRITE_PROTECTED)
        {

            LCD_DisplayStringLine( Line++, (uint8_t*)MSG_WR_PROTECT);

            printf("%s", MSG_WR_PROTECT);
            printf("\r\n");

            LCD_DisplayStringLine( Line++, \
                                   "> HostDemo.TXT could not be created      ");

            printf("> HostDemo.TXT could not be created      ");
            printf("\r\n");

            USBH_USR_ApplicationState = USH_USR_FS_READFILE;
            break;
        }

        if (file_fopen(&file, &efs.myFs, "HOSTDEMO.TXT", 'w') == 0)
        {
            /* Write buffer to file */
            file_write(&file, 100, textFileBuffer);

            LCD_DisplayStringLine( Line++, \
                                   "-----------------------------------------");

            printf("-----------------------------------------");
            printf("\r\n");

            LCD_DisplayStringLine( Line++, \
                                   "HostDemo.TXT successfully created        ");

            printf("HostDemo.TXT successfully created        ");
            printf("\r\n");

            /*close file and filesystem*/
            file_fclose(&file);
            fs_umount(&efs.myFs);

        }

        else
        {
            LCD_DisplayStringLine( Line++, "HostDemo.TXT created in the disk");

            printf("HostDemo.TXT created in the disk");
            printf("\r\n");

            LCD_DisplayStringLine( Line++, \
                                   "To recreate delete HostDemo.TXT in disk ");

            printf("To recreate delete HostDemo.TXT in disk ");
            printf("\r\n");

            USBH_USR_ApplicationState = USH_USR_FS_READFILE;
        }
        break;

    case USH_USR_FS_READFILE:

        /*Reads BMP files, and displays on TFT*/
        LCD_DisplayStringLine( Line++, \
                               "-----------------------------------------");

        printf("-----------------------------------------");
        printf("\r\n");

        LCD_DisplayStringLine( Line++, \
                               "To start Image slide show Press Key ");

        printf("To start Image slide show Press Key ");
        printf("\r\n");

        /*Key in polling*/
        while ( !((HCD_IsDeviceConnected(&USB_OTG_FS_dev)) && \
                  ( !GPIO_ReadInputDataBit(KEYA_PORT, KEYA_PORT_PIN) || GPIO_ReadInputDataBit(KEYB_PORT, KEYB_PORT_PIN) || \
                    GPIO_ReadInputDataBit(KEYC_PORT, KEYC_PORT_PIN)) ) )
        {
            ;
        }

        while (HCD_IsDeviceConnected(&USB_OTG_FS_dev))
        {
            if (efs_init(&efs, 0) != 0)
            {
                /*Could not open filesystem*/
                return -1;
            }

            if (ls_openDir(&list, &(efs.myFs), "/") != 0)
            {
                /*Could not open the selected directory*/
                return -2;
            }

            while ((HCD_IsDeviceConnected(&USB_OTG_FS_dev)) && \
                   (ls_getNext(&list) == 0))
            {
                if (file_fopen(&fileR, &efs.myFs, "picture.bmp", 'r') == 0)
                {
                    bmpImageFound = TRUE;
                    DisplayBMPImage();
                }
                /*close file and filesystem*/
                file_fclose(&fileR);
            }

            if (HCD_IsDeviceConnected(&USB_OTG_FS_dev))
            {
                if (bmpImageFound == TRUE)
                {
                    bmpImageFound = FALSE;
                    /* 5 seconds delay */
                    USB_OTG_BSP_mDelay(5000);
                    return 1;
                }
                else
                {
                    LCD_DisplayStringLine( Line++, \
                                           "-----------------------------------------");

                    printf("-----------------------------------------");
                    printf("\r\n");

                    LCD_DisplayStringLine( Line++, \
                                           "Could not find BMP Images.               ");

                    printf("Could not find BMP Images.               ");
                    printf("\r\n");

                    LCD_DisplayStringLine( Line++, \
                                           "Copy BMP(16bit format) files into the    ");

                    printf("Copy BMP(16bit format) files into the    ");
                    printf("\r\n");

                    LCD_DisplayStringLine( Line++, \
                                           "disk to run image slide show             ");

                    printf("disk to run image slide show             ");
                    printf("\r\n");

                    LCD_DisplayStringLine( Line++, \
                                           "-----------------------------------------");

                    printf("-----------------------------------------");
                    printf("\r\n");

                    /* 5 seconds delay */
                    USB_OTG_BSP_mDelay(5000);

                    return 1; /* Returns 1 when no BMP image found*/
                }
            }
        }
        break;
    default: break;
    }
    return 0;
}



/**
 * @brief  DisplayBMPImage
 *         Displays BMP image
 * @param  None
 * @retval None
 */
void DisplayBMPImage(void)
{
    ImgByteOffset = 0;

    file_fread( &fileR, ImgByteOffset, sizeof(bmp), (uint8_t*)&bmp );

    if ( (bmp.pic_head[0] == 'B') && (bmp.pic_head[1] == 'M') ) /* 如果是BMP图片 */
    {
        uint16_t tx, ty;

        ImgByteOffset = bmp.pic_data_address_h << 16 | bmp.pic_data_address_l;

        for (ty = 0; ty < bmp.pic_h_l; ty++)
        {
            if (!(HCD_IsDeviceConnected(&USB_OTG_FS_dev)))
            {
                return;
            }
            if (HCD_IsDeviceConnected(&USB_OTG_FS_dev))
            {
                file_fread( &fileR, ImgByteOffset, (bmp.pic_w_l) * 3, buffer );
                ImgByteOffset += (bmp.pic_w_l) * 3;
                for ( tx = 0; tx < bmp.pic_w_l; tx++ )
                {
                    point.r = *(tx * 3 + 2 + buffer);
                    point.g = *(tx * 3 + 1 + buffer);
                    point.b = *(tx * 3 + 0 + buffer);
                    point.x = tx;
                    point.y = ty;
                    LCD_SetPoint( point.y, 239 - point.x, RGB565CONVERT(point.r, point.g, point.b) );
                }
            }
        }
    }
}


/**
 * @brief  USBH_USR_HexToASCII
 *         Converts uint16_t type input variable to ASCII digit's array
 * @param  Value to be converted to ASCII
 * @retval None
 */
void USBH_USR_HexToASCII(uint32_t receivedValue)
{
    uint8_t count;
    uint8_t localTemp[10] = "         ";
    uint8_t *ptr;
    uint8_t counter = 0;

    for (count = 0; count < 8; count++ )
    {
        temp[count] = 0x20;
    }

    temp[8] = 0;


    for (count = 0; count < 8; count++ )
    {
        localTemp[count] = receivedValue % 10;


        if (localTemp[count] < 0x0A)
        {
            localTemp[count] += 0x30;
        }
        else
        {
            localTemp[count] += 0x37;
        }

        if (receivedValue < 10)
        {
            break;
        }

        receivedValue = receivedValue / 10;

    }

    count = 0;

    /*check for end of data*/
    ptr = localTemp;
    while (*ptr != 0x20)
    {
        count++;
        ptr++;
    }

    counter = 0;
    ptr--;

    while (count)
    {
        temp[counter] = *ptr;
        ptr--;
        count--;
        counter++;
    }

}

/**
 * @brief  USBH_USR_DeInit
 *         Deint User state and associated variables
 * @param  None
 * @retval None
 */
void USBH_USR_DeInit(void)
{
    USBH_USR_ApplicationState = USH_USR_FS_INIT;
}


/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/






























