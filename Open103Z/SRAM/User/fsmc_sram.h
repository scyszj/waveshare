/*********************************************************************************************************
*
* File                : fsmc_sram.h
* Hardware Environment:
* Build Environment   : RealView MDK-ARM  Version: 4.20
* Version             : V1.0
* By                  :
*
*                                  (c) Copyright 2005-2011, WaveShare
*                                       http://www.waveshare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

#ifndef __FSMC_SRAM_H
#define __FSMC_SRAM_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"

/* Private function prototypes -----------------------------------------------*/
void FSMC_SRAM_Init(void);
void FSMC_SRAM_Test(void);

#endif /* __FSMC_SRAM_H */

/*********************************************************************************************************
      END FILE
*********************************************************************************************************/
