#include "18B20.h"


void convertDs18b20(void)
{
    resetOnewire();
    wOnewire(0xcc);
    wOnewire(0x44);
}


uint8_t* readID(void)
{
    uint8_t ID[8], i, *p;

    resetOnewire();
    wOnewire(0x33);
    for (i = 0; i < 8; i++)
    {
        ID[i] = rOnewire();
    }
    p = ID;
    return p;
}


uint8_t readTemp(void)
{
    uint8_t temp1, temp2;

    convertDs18b20();
    resetOnewire();
    wOnewire(0xcc);
    wOnewire(0xbe);
    temp1 = rOnewire();
    temp2 = rOnewire();
    temp2 = temp2 << 4;
    temp1 = temp1 >> 4;
    temp2 |= temp1;

    return temp2 & 0x7F;
}

