#include "stm32f0xx.h"
#include <stdio.h>
#include "usart.h"
#include "PS2.h"

static volatile uint32_t TimingDelay;
void Delay(uint32_t nTime);
void GPIO_Configuration(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOF, ENABLE);

/**
 *  LED1 -> PF6 , LED2 -> PF7 , LED3 -> PF8 , LED4 -> PF9
 */
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOF, &GPIO_InitStruct);
}
int main(void)
{
    extern u8 rcvF;
    extern u8 keyVal;
    u8 data;
    u8 count;


    RCC_ClocksTypeDef RCC_Clocks;

    /* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500�s */
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

    GPIO_Configuration();
    USART_Configuration();
    NVIC_USART_Config();

    PS2_Init();


    while (1)
    {
        check();
        if (rcvF)
        {
            data = keyHandle(keyVal);
            if (data != 0xff)
            {
                printf("Keyboard Input : %c\r\n", data);
                count++;
                if (count >= 2)
                {
                    count = 0;
                    /*====LED-OFF=======*/
                    GPIO_ResetBits(GPIOF, GPIO_Pin_6);
                    GPIO_ResetBits(GPIOF, GPIO_Pin_7);
                    GPIO_ResetBits(GPIOB, GPIO_Pin_8);
                    GPIO_ResetBits(GPIOB, GPIO_Pin_9);
                }
                else
                {
                    /*====LED-ON=======*/
                    GPIO_SetBits(GPIOF, GPIO_Pin_6);
                    GPIO_SetBits(GPIOF, GPIO_Pin_7);
                    GPIO_SetBits(GPIOB, GPIO_Pin_8);
                    GPIO_SetBits(GPIOB, GPIO_Pin_9);
                }
            }
        }


    }
}
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}


