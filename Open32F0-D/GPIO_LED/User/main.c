#include "stm32f0xx.h"
void  Delay(uint32_t nCount);
int main(void)
{

    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure the LED_pin as output push-pull for LD3 & LD4 usage*/
    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOA, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
    /* Force a low level on LEDs*/
//	GPIO_ResetBits(GPIOB,GPIO_Pin_7);
//	GPIO_SetBits(GPIOB,GPIO_Pin_6);


    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);



    while (1)
    {
        GPIO_ResetBits(GPIOC, GPIO_Pin_9);
        Delay(0xfffff);

        GPIO_SetBits(GPIOC, GPIO_Pin_8);
        Delay(0xfffff);

        GPIO_SetBits(GPIOC, GPIO_Pin_9);
        Delay(0xfffff);

        GPIO_ResetBits(GPIOC, GPIO_Pin_8);
        Delay(0xfffff);


    }
}
void  Delay(uint32_t nCount)
{
    for (; nCount != 0; nCount--)
    {
        ;
    }
}
