/*********************************************************************************************************
*
* File                : PS2.c
* Hardware Environment:
* Build Environment   : ST Visual Develop 4.1.6
* Version             : V1.0
* By                  : Xiao xian hui
*
*                                  (c) Copyright 2005-2010, WaveShare
*                                       http://www.waveShare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

#include "PS2.h"
#include "PS2_code.h"

uint8_t rcvF = 0;
uint8_t keyVal;
/*PS2_DATA->PC6    PS2_CLK->PC7*/
void PS2_OUT_SDA(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /*PS2_DATA->PC6*/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

}

void PS2_IN_SDA(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /*PS2_DATA->PC6*/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void PS2_OUT_SCK(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /*PS2_CLK->PC7*/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void PS2_IN_SCK(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /*PS2_CLK->PC3*/
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);
}

void DELAY(void)
{
    uint8_t i = 10;

    while (i--)
    {
        ;
    }
}

void PS2_Init(void)
{
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
    PS2_IN_SDA();
}

void check(void)
{
    static uint8_t rcvBits = 0;

    PS2_OUT_SCK();
    DELAY();
    SET_SCK;
    DELAY();

    PS2_IN_SCK();
    DELAY();
    if (!GET_SCK)
    {
        if ((rcvBits > 0) && (rcvBits < 9))
        {
            keyVal = keyVal >> 1;
            //IN_SDA;
            //DELAY();
            if (GET_SDA)
            {
                keyVal = keyVal | 0x80;
            }
        }
        rcvBits++;
        while (!GET_SCK)
        {
            ;
        }

        if (rcvBits > 10)
        {
            rcvBits = 0;
            rcvF = 1;
        }
    }
}


uint8_t keyHandle(uint8_t val)
{
    uint8_t i;
    static uint8_t isUp = 0;
    static uint8_t shift = 0;

    rcvF = 0;
    if (isUp == 0)
    {
        switch (val)
        {
        case 0xF0:              // a relase action
            isUp = 1;
            break;
        case 0x12:              // Left shift
            shift = 1;
            break;
        case 0x59:              // Right shift
            shift = 1;
            break;
        default:
            if (shift == 0)         // If shift not pressed
            {

                //for(i=0; unshifted[i][0]!=val && unshifted[i][0];i++);


                for (i = 0; unshifted[i][0] != val && i < 59; i++)
                {
                    ;
                }
                if (unshifted[i][0] == val)
                {
                    //SHIFT_DATA_PORT = val;
                    val = unshifted[i][1];
                    return val;
                }
            }
            else                // If shift pressed
            {
                //for(i=0; unshifted[i][0]!=val && unshifted[i][0]; i++);

                for (i = 0; shifted[i][0] != val && i < 59; i++)
                {
                    ;
                }

                if (shifted[i][0] == val)
                {
                    //SHIFT_DATA_PORT = val;
                    val = shifted[i][1];
                    return val;
                }

            }
        }
    }
    else
    {
        isUp = 0;
        switch (val)
        {
        case 0x12:              // Left SHIFT
            shift = 0;
            break;
        case 0x59:              // Right SHIFT
            shift = 0;
            break;
        }
    }
    return 0xff;
}

