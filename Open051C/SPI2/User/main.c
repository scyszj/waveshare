#include "stm32f0xx.h"
#include <stdio.h>
#include "usart.h"
#include "AT45DBXX.h"

RCC_ClocksTypeDef RCC_Clocks;
static volatile uint32_t TimingDelay;
extern unsigned char DF_buffer[528];

void Delay(uint32_t nTime);


int main(void)
{
    uint8_t num = 0;
    uint8_t ID[4];
    uint8_t data[255], buf[256];

    /* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500�s */
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);
    USART_Configuration();

    AT45DBXX_Init();
    printf("AT45DBXX had been Init!\r\n");
    AT45DBXX_Read_ID(ID);
    printf("AT45DBXX ID is");
    for (num = 0; num < 4; num++)
    {
        printf(" 0x%x ", ID[num]);
    }
    printf("\r\n");

    printf("\r\n EEPROM AT45DBXX Read Test OK\r\n");
    printf("\r\nWrite 255 byte data to buff1:\r\n");
    for (num = 0; num < 255; num++)
    {

        buf[num] = num;
        printf(" %d ", buf[num]);
    }
    DF_write_buf(1, 1, buf, 255);
    DF_buf_to_mm(1, 10);
    printf("\r\nRead 255 byte data from buff1:\r\n");
    DF_mm_to_buf(1, 10);
    DF_read_buf(1, 1, data, 255);
    for (num = 0; num < 255; num++)
    {

        printf(" %d ", data[num]);
    }

    while (1)
    {

        Delay(100);
    }

}
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}


