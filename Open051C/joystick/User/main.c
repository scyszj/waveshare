#include "stm32f0xx.h"
#include <stdio.h>
#include "usart.h"

RCC_ClocksTypeDef RCC_Clocks;
static volatile uint32_t TimingDelay;
void Delay(uint32_t nTime);
void  Init_GPIOs(void);
int main(void)
{
    uint8_t Key;

    /* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500�s */
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

    Init_GPIOs();
    GPIO_SetBits(GPIOB, GPIO_Pin_8);
    GPIO_ResetBits(GPIOB, GPIO_Pin_9);
    GPIO_SetBits(GPIOF, GPIO_Pin_6);
    GPIO_ResetBits(GPIOF, GPIO_Pin_7);
    Delay(100);
//	USART_Configuration();
    while (1)
    {
        if (!(GPIOA->IDR & 0x0001))
        {
            Key = 1;
        }
        else if (!(GPIOA->IDR & 0x0002))
        {
            Key = 2;
        }
        else if (!(GPIOB->IDR & 0x0001))
        {
            Key = 3;
        }
        else if (!(GPIOB->IDR & 0x0002))
        {
            Key = 4;
        }
        else if (!(GPIOB->IDR & 0x0004))
        {
            Key = 5;
        }
        else
        {
            Key = 0;
        }
        switch (Key)
        {
        case 1: GPIO_SetBits(GPIOB, GPIO_Pin_8);     Delay(50); break;
        case 2:   GPIO_SetBits(GPIOB, GPIO_Pin_9);     Delay(50); break;
        case 3:   GPIO_SetBits(GPIOF, GPIO_Pin_6);   Delay(50); break;
        case 4: GPIO_SetBits(GPIOF, GPIO_Pin_7);       Delay(50); break;
        case 5:   GPIO_SetBits(GPIOB, GPIO_Pin_8 | GPIO_Pin_9);
            GPIO_SetBits(GPIOF, GPIO_Pin_6 | GPIO_Pin_7);      Delay(50); break;

        default: GPIO_ResetBits(GPIOB, GPIO_Pin_8);
            GPIO_ResetBits(GPIOB, GPIO_Pin_9);
            GPIO_ResetBits(GPIOF, GPIO_Pin_6);
            GPIO_ResetBits(GPIOF, GPIO_Pin_7);
        }

    }

}

void  Init_GPIOs(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure the LED_pin as output push-pull for LD3 & LD4 usage*/
    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOF, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOF, &GPIO_InitStructure);

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE);
    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE);
    /* Key */

    GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_0 | GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

}
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}


