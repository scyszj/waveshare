

#ifndef _AT45DBXX_H
#define _AT45DBXX_H //1

#include "stm32f0xx.h"
#include "ws_spi.h"

/*DATA_FLASH端口及指令定义*/
/*
   #define DF_SPI_ON			  SPCR = 0x5F
   #define DF_SPI_OFF			SPCR = 0x00
   #define DF_CON_PORT			PORTC
   #define DF_STA_PORT			PINC
   #define DF_STATE			  PC2
   #define DF_CS_1				  PC1
   #define DF_SELECT_1			DF_CON_PORT &=~_BV(DF_CS_1)
   #define DF_DESELECT_1		DF_CON_PORT |=_BV(DF_CS_1)
 */

#define RCC_GPIO_CS                 RCC_AHB1Periph_GPIOA
#define RCC_AHBxPeriphClockCmd      RCC_AHB1PeriphClockCmd
#define GPIO_PIN_CS                 GPIO_Pin_4
#define GPIO_CS_PORT                GPIOA


#define DF_SELECT_1       GPIO_ResetBits(GPIOA, GPIO_Pin_4);
#define DF_DESELECT_1   GPIO_SetBits(GPIOA, GPIO_Pin_4);


#define BUFFER_1_WRITE                    0x84      // 写入第一缓冲区
#define BUFFER_2_WRITE                    0x87      // 写入第二缓冲区
#define BUFFER_1_READ                     0xD4      // 读取第一缓冲区
#define BUFFER_2_READ                     0xD6      // 读取第二缓冲区
#define B1_TO_MM_PAGE_PROG_WITH_ERASE     0x83    // 将第一缓冲区的数据写入主存储器（擦除模式）
#define B2_TO_MM_PAGE_PROG_WITH_ERASE     0x86    // 将第二缓冲区的数据写入主存储器（擦除模式）
#define MM_PAGE_TO_B1_XFER                0x53      // 将主存储器的指定页数据加载到第一缓冲区
#define MM_PAGE_TO_B2_XFER                0x55      // 将主存储器的指定页数据加载到第二缓冲区
#define PAGE_ERASE                        0x81      // 页删除（每页512/528字节）
#define SECTOR_ERASE                      0x7C      // 扇区擦除（每扇区128K字节）
#define READ_STATE_REGISTER               0xD7      // 读取状态寄存器
#define Read_ID                           0x9F

extern unsigned char DF_buffer[528];

void AT45DBXX_Init(void);
void AT45DBXX_Read_ID(uint8_t *IData);
//void write_SPI				(unsigned char data);
void DF_format(void);
void DF_page_earse(unsigned int page);
void DF_write_page(unsigned int page);
void DF_read_page(unsigned int page);
void DF_mm_read_page(unsigned int page);
unsigned char DF_read_reg(void);
void DF_wait_busy(void);
void DF_mm_to_buf(unsigned char buffer, unsigned int page);
unsigned char DF_read_buf(unsigned char buffer, unsigned int start_address, unsigned char *data, unsigned int length);
unsigned char DF_write_buf(unsigned char buffer, unsigned int start_address, unsigned char *data, unsigned int length);
void DF_buf_to_mm(unsigned char buffer, unsigned int page);


#endif
