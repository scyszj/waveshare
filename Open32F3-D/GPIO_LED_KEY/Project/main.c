#include "main.h"
/* Private variables ---------------------------------------------------------*/
USART_InitTypeDef USART_InitStructure;
static __IO uint32_t TimingDelay;
RCC_ClocksTypeDef RCC_Clocks;


/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE * f)
#endif /* __GNUC__ */

/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
int main(void)
{
    unsigned char Key;

    /*!< At this stage the microcontroller clock setting is already configured,
         this is done through SystemInit() function which is called from startup
         file (startup_stm32f30x.s) before to branch to application main.
         To reconfigure the default setting of SystemInit() function, refer to
         system_stm32f30x.c file
     */
    /* SysTick end of count event each 10ms */


    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

    /* Initialize LEDs, Key Button, LCD and COM port(USART) available on
       STM32303C-EVAL board *****************************************************/

    Init_key();


    while (1)
    {
        if (!(GPIOF->IDR & 0x0004))
        {
            Key = 1;
        }
        else if (!(GPIOF->IDR & 0x0010))
        {
            Key = 2;
        }
        else if (!(GPIOF->IDR & 0x0040))
        {
            Key = 3;
        }
        else if (!(GPIOF->IDR & 0x0200))
        {
            Key = 4;
        }
        else if (!(GPIOF->IDR & 0x0400))
        {
            Key = 5;
        }
//      else if((GPIOA->IDR & 0x0001))
//          Key=6;
        switch (Key)
        {
        case 1:   STM_EVAL_LEDToggle(LED8); Delay(8); break;
        case 2:   STM_EVAL_LEDToggle(LED2); Delay(8); break;
        case 3:   STM_EVAL_LEDToggle(LED6); Delay(8); break;
        case 4:   STM_EVAL_LEDToggle(LED4); Delay(8); break;
        case 5:   STM_EVAL_LEDToggle(LED1); STM_EVAL_LEDToggle(LED2);
            STM_EVAL_LEDToggle(LED3);   STM_EVAL_LEDToggle(LED4);
            STM_EVAL_LEDToggle(LED5);   STM_EVAL_LEDToggle(LED6);
            STM_EVAL_LEDToggle(LED7);   STM_EVAL_LEDToggle(LED8);
            Delay(8); break;
//       case 6:   STM_EVAL_LEDToggle(LED1);Delay(3);STM_EVAL_LEDToggle(LED2);Delay(3);
//          STM_EVAL_LEDToggle(LED3);	Delay(3);STM_EVAL_LEDToggle(LED4);Delay(3);
//          STM_EVAL_LEDToggle(LED5);	Delay(3);STM_EVAL_LEDToggle(LED6);	Delay(3);
//          STM_EVAL_LEDToggle(LED7);	Delay(3);STM_EVAL_LEDToggle(LED8);
//          Delay(8);break;

        }
        GPIOE->ODR = GPIO_Pin_All; Delay(20);
    }

}
void Init_key(void)
{


    GPIO_InitTypeDef GPIO_InitStructure;

    /* Enable the GPIO_LED Clock */
    RCC_AHBPeriphClockCmd(KEY_BUTTON_GPIO_CLK, ENABLE);

    /* Configure the GPIO_LED pin */
    GPIO_InitStructure.GPIO_Pin = KEY_BUTTON_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(KEY_BUTTON_GPIO_PORT, &GPIO_InitStructure);

    /* Configure the GPIO_LED pin */
    RCC_AHBPeriphClockCmd(RIGHT_BUTTON_GPIO_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Pin = RIGHT_BUTTON_PIN | LEFT_BUTTON_PIN | UP_BUTTON_PIN | DOWN_BUTTON_PIN | SEL_BUTTON_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(RIGHT_BUTTON_GPIO_PORT, &GPIO_InitStructure);


    STM_EVAL_LEDInit(LED1);
    STM_EVAL_LEDInit(LED2);
    STM_EVAL_LEDInit(LED3);
    STM_EVAL_LEDInit(LED4);
    STM_EVAL_LEDInit(LED5);
    STM_EVAL_LEDInit(LED6);
    STM_EVAL_LEDInit(LED7);
    STM_EVAL_LEDInit(LED8);
}
/**
 * @brief  Retargets the C library printf function to the USART.
 * @param  None
 * @retval None
 */
PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(EVAL_COM1, (uint8_t)ch);

    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(EVAL_COM1, USART_FLAG_TC) == RESET)
    {
    }

    return ch;
}

/**
 * @brief  Inserts a delay time.
 * @param  nTime: specifies the delay time length, in 10 ms.
 * @retval None
 */
void Delay(__IO uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }
}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{
    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
 * @}
 */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
