/*********************************************************************************************************
*
* File                : main.c
* Hardware Environment:
* Build Environment   : RealView MDK-ARM  Version: 4.60
* Version             : V1.0
* By                  :
*
*                                  (c) Copyright 2005-2013, WaveShare
*                                       http://www.waveshare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stdlib.h>

#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE * f)
#endif /* __GNUC__ */
USART_InitTypeDef USART_InitStructure;
RCC_ClocksTypeDef RCC_Clocks;
/* Private variables ---------------------------------------------------------*/
uint16_t CAN_ID;
uint8_t CAN_DATA0, CAN_DATA1, CAN_DATA2, CAN_DATA3, CAN_DATA4, CAN_DATA5, CAN_DATA6, CAN_DATA7;
uint8_t CanFlag, Display;
static __IO uint32_t TimingDelay;
/* Private function prototypes -----------------------------------------------*/
void GPIO_Configuration(void);
void USART_Configuration(void);
void NVIC_Configuration(void);
void CAN_Configuration(void);
void CanWriteData(uint16_t ID);

/*******************************************************************************
* Function Name  : Delay
* Description    : Delay Time
* Input          : - nCount: Delay Time
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/

/*******************************************************************************
* Function Name  : main
* Description    : Main program
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
int main(void)
{

    CAN_Configuration();
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    STM_EVAL_COMInit(COM1, &USART_InitStructure);

    printf("\r\n****************************************************************\r\n");

    printf("CAN-Bus Test \r\n");
    printf("CAN-Bus Speed 100kHz \r\n");
    /* Infinite loop */
    while (1)
    {
        if ( CanFlag == ENABLE )
        {
            CanFlag = DISABLE;
            printf("CAN Receive Data \r\n");
            printf("CAN ID %x \r\n", CAN_ID);
            printf("CAN_DATA0 %x \r\n", CAN_DATA0);
            printf("CAN_DATA1 %x \r\n", CAN_DATA1);
            printf("CAN_DATA2 %x \r\n", CAN_DATA2);
            printf("CAN_DATA3 %x \r\n", CAN_DATA3);
            printf("CAN_DATA4 %x \r\n", CAN_DATA4);
            printf("CAN_DATA5 %x \r\n", CAN_DATA5);
            printf("CAN_DATA6 %x \r\n", CAN_DATA6);
            printf("CAN_DATA7 %x \r\n", CAN_DATA7);
        }

        CanWriteData(0xA5A5);

        if ( Display )
        {
            /*====LED-ON=======*/
            GPIO_SetBits(GPIOC, GPIO_Pin_9);
            GPIO_SetBits(GPIOC, GPIO_Pin_10);
            GPIO_SetBits(GPIOC, GPIO_Pin_11);
            GPIO_SetBits(GPIOC, GPIO_Pin_12);
        }
        else
        {
            /*====LED-OFF=======*/
            GPIO_ResetBits(GPIOC, GPIO_Pin_9);
            GPIO_ResetBits(GPIOC, GPIO_Pin_10);
            GPIO_ResetBits(GPIOC, GPIO_Pin_11);
            GPIO_ResetBits(GPIOC, GPIO_Pin_12);
        }
        Display = ~Display;
        Delay(50);
    }
}

/*******************************************************************************
* Function Name  : GPIO_Configuration
* Description    : Configures the different GPIO ports.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void GPIO_Configuration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* CAN Periph clock enable */
    /* Enable GPIO clock */
    RCC_AHBPeriphClockCmd(CAN_GPIO_CLK, ENABLE);

    /* Connect CAN pins to AF7 */
    GPIO_PinAFConfig(CAN_GPIO_PORT, CAN_RX_SOURCE, CAN_AF_PORT);
    GPIO_PinAFConfig(CAN_GPIO_PORT, CAN_TX_SOURCE, CAN_AF_PORT);

    /* Configure CAN RX and TX pins */
    GPIO_InitStructure.GPIO_Pin = CAN_RX_PIN | CAN_TX_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
    GPIO_Init(CAN_GPIO_PORT, &GPIO_InitStructure);


}

/*******************************************************************************
* Function Name  : NVIC_Configuration
* Description    : Configures the nested vectored interrupt controller.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void NVIC_Configuration(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Enable CAN1 RX0 interrupt IRQ channel */
    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/*******************************************************************************
* Function Name  : CAN_Configuration
* Description    : Configures the CAN
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void CAN_Configuration(void)
{
    CAN_InitTypeDef CAN_InitStructure;
    CAN_FilterInitTypeDef CAN_FilterInitStructure;

    RCC_APB1PeriphClockCmd(CAN_CLK, ENABLE);
    NVIC_Configuration();
    GPIO_Configuration();
    /* CAN register init */
    CAN_DeInit(CAN1);
    CAN_StructInit(&CAN_InitStructure);

    /* CAN cell init */
    CAN_InitStructure.CAN_TTCM = DISABLE;
    CAN_InitStructure.CAN_ABOM = DISABLE;
    CAN_InitStructure.CAN_AWUM = DISABLE;
    CAN_InitStructure.CAN_NART = DISABLE;
    CAN_InitStructure.CAN_RFLM = DISABLE;
    CAN_InitStructure.CAN_TXFP = ENABLE;
    CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
    CAN_InitStructure.CAN_SJW = CAN_SJW_1tq;
    CAN_InitStructure.CAN_BS1 = CAN_BS1_4tq;
    CAN_InitStructure.CAN_BS2 = CAN_BS2_3tq;
    CAN_InitStructure.CAN_Prescaler = 45;

    if (CAN_Init(CAN1, &CAN_InitStructure) == CANINITFAILED)
    {
    }

    CAN_FilterInitStructure.CAN_FilterNumber = 0;
    CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
    CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
    CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
    CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
    CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
    CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
    CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
    CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);
    CAN_FilterInit(&CAN_FilterInitStructure);
}

/*******************************************************************************
* Function Name  : CanWriteData
* Description    : Can Write Date to CAN-BUS
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void CanWriteData(uint16_t ID)
{
    CanTxMsg TxMessage;

    CAN_DATA0 = rand() % 0xff;  CAN_DATA1 = rand() % 0xff;
    CAN_DATA2 = rand() % 0xff;  CAN_DATA3 = rand() % 0xff;
    CAN_DATA4 = rand() % 0xff;  CAN_DATA5 = rand() % 0xff;
    CAN_DATA6 = rand() % 0xff;  CAN_DATA7 = rand() % 0xff;

    /* transmit */
    TxMessage.StdId = ID;
//TxMessage.ExtId = 0x00;
    TxMessage.RTR = CAN_RTR_DATA;
    TxMessage.IDE = CAN_ID_STD;
    TxMessage.DLC = 8;
    TxMessage.Data[0] = CAN_DATA0;
    TxMessage.Data[1] = CAN_DATA1;
    TxMessage.Data[2] = CAN_DATA2;
    TxMessage.Data[3] = CAN_DATA3;
    TxMessage.Data[4] = CAN_DATA4;
    TxMessage.Data[5] = CAN_DATA5;
    TxMessage.Data[6] = CAN_DATA6;
    TxMessage.Data[7] = CAN_DATA7;
    CAN_Transmit(CAN1, &TxMessage);
}

/*******************************************************************************
* Function Name  : USB_LP_CAN1_RX0_IRQHandler
* Description    : This function handles USB Low Priority or CAN RX0 interrupts
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void USB_LP_CAN1_RX0_IRQHandler(void)
{
    CanRxMsg RxMessage;

    CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);
    CAN_ID = RxMessage.StdId;
    CAN_DATA0 = RxMessage.Data[0];
    CAN_DATA1 = RxMessage.Data[1];
    CAN_DATA2 = RxMessage.Data[2];
    CAN_DATA3 = RxMessage.Data[3];
    CAN_DATA4 = RxMessage.Data[4];
    CAN_DATA5 = RxMessage.Data[5];
    CAN_DATA6 = RxMessage.Data[6];
    CAN_DATA7 = RxMessage.Data[7];
    CAN_ClearITPendingBit(CAN1, CAN_IT_FMP0);
    CanFlag = ENABLE;
}

void Delay(__IO uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }
}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{
    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(EVAL_COM1, (uint8_t)ch);

    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(EVAL_COM1, USART_FLAG_TC) == RESET)
    {
    }

    return ch;
}
/*********************************************************************************************************
      END FILE
*********************************************************************************************************/

