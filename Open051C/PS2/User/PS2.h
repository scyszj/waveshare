/*********************************************************************************************************
*
* File                : PS2.h
* Hardware Environment:
* Build Environment   : RealView MDK-ARM Version:4.14
* Version             : V1.0
* By                  : Xiao xian hui
*
*                                  (c) Copyright 2005-2010, WaveShare
*                                       http://www.waveShare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

#ifndef _PS2_H
#define _PS2_H

#include "stm32f0xx.h"

#define u8 unsigned char
#define SET_SDA     GPIO_SetBits(GPIOA, GPIO_Pin_11)
#define CLR_SDA     GPIO_ResetBits(GPIOA, GPIO_Pin_11)
#define GET_SDA     GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_11)

#define SET_SCK     GPIO_SetBits(GPIOA, GPIO_Pin_12)
#define CLR_SCK     GPIO_ResetBits(GPIOA, GPIO_Pin_12)
#define GET_SCK     GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_12)

void PS2_OUT_SDA(void);
void PS2_IN_SDA(void);
void PS2_OUT_SCK(void);
void PS2_IN_SCK(void);
void PS2_Init(void);
void check(void);
u8 keyHandle(u8 val);

#endif /*_PS2_H*/
