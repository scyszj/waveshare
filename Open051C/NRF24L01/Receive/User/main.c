#include "stm32f0xx.h"
#include "usart.h"
#include "NRF24L01.h"


//#define T_O_R	1			//Send

#define T_O_R   0           //Receive

extern uint8_t RX_BUF[];
extern uint8_t TX_BUF[];

void GPIO_Configuration_key(void);
void LED_indicate(uint8_t value);

RCC_ClocksTypeDef RCC_Clocks;
static volatile uint32_t TimingDelay;
void RTC_Configuration(void);
void RCC_Configuration(void);
void Delay(uint32_t nTime);
void USART_Configuration(void);

int main(void)
{

    /* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500祍 */
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

//	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF,ENABLE);
//	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
//	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
//	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
//	GPIO_Init(GPIOF, &GPIO_InitStruct);
//	GPIO_WriteBit(GPIOF, GPIO_Pin_6 | GPIO_Pin_7, Bit_RESET);
    USART_Configuration();
    printf("USART1测试成功!");
    nRF24L01_Initial();
    printf("NRF2401初始化成功!\r\n");

    GPIO_Configuration_key();
//	printf("\r\n接收到数据：%s\r\n",RX_BUF1);
    if (T_O_R)
    {
        TX_BUF[0] = 1;

        printf("nRF24L01发送模式\r\n");
    }
    else
    {
        RX_Mode();
        printf("nRF24L01接收模式\r\n");
    }
    while (1)
    {
        Delay(100);
        if (T_O_R)
        {
            NRF24L01_Send();
            Delay(100);
        }


        else
        {
            NRF24L01_Receive();
            Delay(100);


        }
    }

}

void GPIO_Configuration_key(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure the LED_pin as output push-pull for LD3 & LD4 usage*/
    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE);

    /* Key */

    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

}

void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}
