/********************************************************************************************************
 *
 * File                : main.c
 * Hardware Environment:
 * Build Environment   : RealView MDK-ARM  Version: 4.14
 * Version             : V1.0
 * By                  :
 *
 *                                  (c) Copyright 2005-2011, WaveShare
 *                                       http://www.waveShare.net
 *                                          All Rights Reserved
 *
 *********************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "usbh_usr.h"
#include "usbh_core.h"
#include "usbh_msc_core.h"

#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE * f)
#endif /* __GNUC__ */

/*******************************************************************************
* Function Name  : main
* Description    : Main program
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
int main(void)
{
    /* At this point System clock is already set to 72MHz
       using CMSIS SystemInit() function */

    /* STM32 evalboard user initializations*/
    BSP_Init();

    /* Init Host Library */
    USBH_Init(&USB_OTG_FS_dev, &MSC_cb, &USR_Callbacks);

    while (1)
    {
        /* Host Task handler */
        USBH_Process();
    }
}

/**
 * @brief  Retargets the C library printf function to the USART.
 * @param  None
 * @retval None
 */
PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(USART1, (uint8_t)ch);

    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
    {
    }

    return ch;
}


#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/*********************************************************************************************************
      END FILE
*********************************************************************************************************/
