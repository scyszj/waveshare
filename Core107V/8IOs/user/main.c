#include <stm32f10x.h>
#include "usart.h"

void GPIO_Configuration(void);
void  Delay(uint32_t nCount);

int main(void)
{
    usart_Configuration();
    GPIO_Configuration();
    printf("Welcome to use Port103V development board!\r\n");
    printf("Please put the 8 push Buttons from 8 on IOs module, then press the button!!\r\n");
    while (1)
    {
        if (!(GPIOD->IDR & (1 << 0)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 0)))
            {
                printf("\r\nYou enter the key value:KY7\r\n");
                while (!(GPIOD->IDR & (1 << 0)))
                {
                    ;
                }
            }
        }
        else if (!(GPIOD->IDR & (1 << 1)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 1)))
            {
                printf("\r\nYou enter the key value:KY6\r\n");
                while (!(GPIOD->IDR & (1 << 1)))
                {
                    ;
                }
            }
        }
        else if (!(GPIOD->IDR & (1 << 2)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 2)))
            {
                printf("\r\nYou enter the key value:KY5\r\n");
                while (!(GPIOD->IDR & (1 << 2)))
                {
                    ;
                }
            }
        }
        else if (!(GPIOD->IDR & (1 << 3)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 3)))
            {
                printf("\r\nYou enter the key value:KY4\r\n");
                while (!(GPIOD->IDR & (1 << 3)))
                {
                    ;
                }
            }
        }

        else if (!(GPIOD->IDR & (1 << 4)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 4)))
            {
                printf("\r\nYou enter the key value:KY3\r\n");
                while (!(GPIOD->IDR & (1 << 4)))
                {
                    ;
                }
            }
        }

        else if (!(GPIOD->IDR & (1 << 5)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 5)))
            {
                printf("\r\nYou enter the key value:KY2\r\n");
                while (!(GPIOD->IDR & (1 << 5)))
                {
                    ;
                }
            }
        }
        else if (!(GPIOD->IDR & (1 << 6)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 6)))
            {
                printf("\r\nYou enter the key value:KY1\r\n");
                while (!(GPIOD->IDR & (1 << 6)))
                {
                    ;
                }
            }
        }
        else if (!(GPIOD->IDR & (1 << 7)))
        {
            Delay(0xfffff);
            if (!(GPIOD->IDR & (1 << 7)))
            {
                printf("\r\nYou enter the key value:KY0\r\n");
                while (!(GPIOD->IDR & (1 << 7)))
                {
                    ;
                }
            }
        }

    }

}

void GPIO_Configuration(void)
{
//PD0 PD1 PD2 PD3 PD4 PD5 PD6 PD7

    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD, ENABLE);

    /* Key */
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOD, &GPIO_InitStructure);
}
/*******************************************************************************
* Function Name  : Delay
* Description    : Delay Time
* Input          : - nCount: Delay Time
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void  Delay(uint32_t nCount)
{
    for (; nCount != 0; nCount--)
    {
        ;
    }
}