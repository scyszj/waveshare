#include "main.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

  #define MESSAGE1   "STM32F30x CortexM4  "
  #define MESSAGE2   " Device running on  "
  #define MESSAGE3   "   Open32F3-D       "

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
USART_InitTypeDef USART_InitStructure;
static __IO uint32_t TimingDelay;
RCC_ClocksTypeDef RCC_Clocks;

/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE * f)
#endif /* __GNUC__ */

/* Private functions ---------------------------------------------------------*/
void  Init_GPIOs(void);
uint8_t Key;
/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
int main(void)
{
    /*!< At this stage the microcontroller clock setting is already configured,
         this is done through SystemInit() function which is called from startup
         file (startup_stm32f30x.s) before to branch to application main.
         To reconfigure the default setting of SystemInit() function, refer to
         system_stm32f30x.c file
     */
    /* SysTick end of count event each 10ms */

    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

    /* Initialize LEDs, Key Button, LCD and COM port(USART) available on
       STM32303C-EVAL board *****************************************************/
    STM_EVAL_LEDInit(LED1);
    STM_EVAL_LEDInit(LED2);
    STM_EVAL_LEDInit(LED3);
    STM_EVAL_LEDInit(LED4);

    /* USARTx configured as follow:
          - BaudRate = 115200 baud
          - Word Length = 8 Bits
          - One Stop Bit
          - No parity
          - Hardware flow control disabled (RTS and CTS signals)
          - Receive and transmit enabled
     */
    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    STM_EVAL_COMInit(COM1, &USART_InitStructure);

    /* Retarget the C library printf function to the USARTx, can be USART2 or USART3
       depending on the EVAL board you are using ********************************/
    printf("\n\r %s", MESSAGE1);
    printf(" %s", MESSAGE2);
    printf(" %s\n\r", MESSAGE3);

    /* Add your application code here
     */
    Init_GPIOs();

    /* Infinite loop */
    while (1)
    {

        if (!(GPIOC->IDR & 0x0001))
        {
            Key = 1;
        }
        else if (!(GPIOC->IDR & 0x0002))
        {
            Key = 2;
        }
        else if (!(GPIOC->IDR & 0x0004))
        {
            Key = 3;
        }
        else if (!(GPIOC->IDR & 0x0008))
        {
            Key = 4;
        }
        else if (!(GPIOC->IDR & 0x0010))
        {
            Key = 5;
        }
        else if (!(GPIOC->IDR & 0x0020))
        {
            Key = 6;
        }
        else if (!(GPIOC->IDR & 0x0040))
        {
            Key = 7;
        }
        else if (!(GPIOC->IDR & 0x0080))
        {
            Key = 8;
        }



        switch (Key)
        {
        case 1: printf("key0\r\n"); Delay(50); break;
        case 2: printf("key1\r\n"); Delay(50); break;
        case 3: printf("key2\r\n"); Delay(50); break;
        case 4: printf("key3\r\n"); Delay(50); break;
        case 5: printf("key4\r\n"); Delay(50); break;
        case 6: printf("key5\r\n"); Delay(50); break;
        case 7: printf("key6\r\n"); Delay(50); break;
        case 8: printf("key7\r\n"); Delay(50); break;
        }
        Key = 0;
    }
}
void  Init_GPIOs(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOC, ENABLE);

    /* Key */

    GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_0 | GPIO_Pin_1 |  GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);



}
/**
 * @brief  Retargets the C library printf function to the USART.
 * @param  None
 * @retval None
 */
PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(EVAL_COM1, (uint8_t)ch);

    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(EVAL_COM1, USART_FLAG_TC) == RESET)
    {
    }

    return ch;
}

/**
 * @brief  Inserts a delay time.
 * @param  nTime: specifies the delay time length, in 10 ms.
 * @retval None
 */
void Delay(__IO uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }
}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{
    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif

/**
 * @}
 */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
