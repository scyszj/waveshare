/*********************************************************************************************************
*
* File                : PS2.h
* Hardware Environment:
* Build Environment   : RealView MDK-ARM Version:4.14
* Version             : V1.0
* By                  : Xiao xian hui
*
*                                  (c) Copyright 2005-2010, WaveShare
*                                       http://www.waveShare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

#ifndef _PS2_H
#define _PS2_H

#include "stm32f30x.h"

#define SET_SDA     GPIO_SetBits(GPIOE, GPIO_Pin_6)
#define CLR_SDA     GPIO_ResetBits(GPIOE, GPIO_Pin_6)
#define GET_SDA     GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_6)

#define SET_SCK     GPIO_SetBits(GPIOE, GPIO_Pin_7)
#define CLR_SCK     GPIO_ResetBits(GPIOE, GPIO_Pin_7)
#define GET_SCK     GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_7)

void PS2_OUT_SDA(void);
void PS2_IN_SDA(void);
void PS2_OUT_SCK(void);
void PS2_IN_SCK(void);
void PS2_Init(void);
void check(void);
uint8_t keyHandle(uint8_t val);

#endif /*_PS2_H*/
