/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stdio.h>
/** @addtogroup STM32303C_EVAL_Demo
 * @{
 */

/** @defgroup HBLed_LDR
 * @brief    This file includes the about driver for the STM32303C-EVAL
 *           demonstration.
 * @{
 */

/** @defgroup HBLed_LDR_Private_Types
 * @{
 */

/**
 * @}
 */


/** @defgroup HBLed_LDR_Private_Defines
 * @{
 */

/**
 * @}
 */

/** @defgroup HBLed_LDR_Private_Macros
 * @{
 */


/**
 * @}
 */
uint16_t ADC_ConvResult = 0;
uint8_t HB_Level = 0;
uint8_t HB_Led = 0;
__IO uint8_t LedDrvTrig = 0;


static void LDR_Init(void);



/**
 * @brief  Main program.
 * @param  None
 * @retval None
 */
void Menu_BrightnessLed_LDRFunc(void)
{
    uint32_t ldrlevel = 11, daclevel = 0;


    /* Set value */
    HB_Led = 1;


    /* LDR initialization */
    LDR_Init();



    /* Infinite loop */
    while (1)
    {

        for (daclevel = 0; daclevel < 11; daclevel++)
        {
            /* Set DAC Channel1 DHR register */
            DAC_SetChannel1Data(DAC_Align_12b_R, (uint16_t)(daclevel * 300));

            Delay(1);
            /* Check on the Comp output level*/
            if (COMP_GetOutputLevel(COMP_Selection_COMP7) == COMP_OutputLevel_High)
            {
                ldrlevel--;
            }

        }
        printf("\n\rldrlevel:%d\n\r",  ldrlevel);
        /* Toggle LD1 */
        //   STM_EVAL_LEDToggle(LED1);

//     /* Insert 50 ms delay */
//     Delay(5);

//     /* Toggle LD2 */
//     STM_EVAL_LEDToggle(LED2);

//     /* Insert 50 ms delay */
//     Delay(5);

//     /* Toggle LD3 */
//     STM_EVAL_LEDToggle(LED3);

//     /* Insert 50 ms delay */
//     Delay(5);

//     /* Toggle LD4 */
//     STM_EVAL_LEDToggle(LED4);

//     /* Insert 50 ms delay */
//     Delay(5);
        ldrlevel = 11;
    }


}

/**
 * @brief LDR Configuration.
 * @param None
 * @retval None
 */
static void LDR_Init(void)
{
    COMP_InitTypeDef COMP_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    DAC_InitTypeDef DAC_InitStructure;

    /* DAC clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);
    /* Enable SYSCFG clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /* Configure PA0 in analog mode: PA0 is connected to COMP7 non inverting input */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* COMP7 Init:use DAC1 output as a reference voltage: DAC1 output is connected
       to COMP7 inverting input */
    COMP_InitStructure.COMP_NonInvertingInput = COMP_NonInvertingInput_IO1;
    COMP_InitStructure.COMP_InvertingInput = COMP_InvertingInput_DAC1;
    COMP_InitStructure.COMP_Output = COMP_Output_None;
    COMP_InitStructure.COMP_OutputPol = COMP_OutputPol_NonInverted;
    COMP_InitStructure.COMP_BlankingSrce = COMP_BlankingSrce_None;
    COMP_InitStructure.COMP_Hysteresis = COMP_Hysteresis_High;
    COMP_InitStructure.COMP_Mode = COMP_Mode_UltraLowPower;
    COMP_Init(COMP_Selection_COMP7, &COMP_InitStructure);

    /* Enable the COMP peripheral */
    COMP_Cmd(COMP_Selection_COMP7, ENABLE);

    /* Deinitialize DAC */
    DAC_DeInit();
    /* DAC Channel1 Init */
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
    DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0;
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Disable;
    DAC_Init(DAC_Channel_1, &DAC_InitStructure);

    /* Enable DAC Channel1 */
    DAC_Cmd(DAC_Channel_1, ENABLE);
}
