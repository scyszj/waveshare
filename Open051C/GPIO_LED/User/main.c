#include "stm32f0xx.h"
void  Delay(uint32_t nCount);
int main(void)
{

    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure the LED_pin as output push-pull for LD3 & LD4 usage*/
    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOF, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    /* Force a low level on LEDs*/
//	GPIO_ResetBits(GPIOB,GPIO_Pin_7);
//	GPIO_SetBits(GPIOB,GPIO_Pin_6);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOF, &GPIO_InitStructure);

    while (1)
    {

        GPIO_SetBits(GPIOB, GPIO_Pin_8);
        Delay(0xfffff);

        GPIO_SetBits(GPIOB, GPIO_Pin_9);
        Delay(0xfffff);

        GPIO_SetBits(GPIOF, GPIO_Pin_6);
        Delay(0xfffff);

        GPIO_SetBits(GPIOF, GPIO_Pin_7);
        Delay(0xfffff);


        GPIO_ResetBits(GPIOB, GPIO_Pin_8);
        Delay(0xfffff);

        GPIO_ResetBits(GPIOB, GPIO_Pin_9);
        Delay(0xfffff);

        GPIO_ResetBits(GPIOF, GPIO_Pin_6);
        Delay(0xfffff);

        GPIO_ResetBits(GPIOF, GPIO_Pin_7);
        Delay(0xfffff);

    }
}
void  Delay(uint32_t nCount)
{
    for (; nCount != 0; nCount--)
    {
        ;
    }
}
