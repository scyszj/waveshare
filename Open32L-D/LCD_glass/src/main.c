#include "stm32l1xx.h"
#include "stdio.h"
#include "discover_board.h"
#include "stm32l_discovery_lcd.h"

#define DEBUG_SWD_PIN

//#define GPIO_HIGH(a,b)        a->BSRRL = b
//#define GPIO_LOW(a,b)		a->BSRRH = b
//#define GPIO_TOGGLE(a,b)  a->ODR ^= b
//
//
//#define USERBUTTON_GPIO_PORT	GPIOA
//#define USERBUTTON_GPIO_PIN     GPIO_Pin_0
//#define USERBUTTON_GPIO_CLK     RCC_AHBPeriph_GPIOA


extern uint8_t t_bar[2];

static volatile uint32_t TimingDelay;
RCC_ClocksTypeDef RCC_Clocks;
char strDisp[20];
void RTC_Configuration(void);
void RCC_Configuration(void);
void Init_GPIOs(void);
void Delay(uint32_t nTime);

int main(void)
{

    RCC_Configuration();

    RTC_Configuration();
    /* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500�s */
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 2000);

    /* Init I/O ports */
    /* Init I/O ports */
    Init_GPIOs();

    /* Initializes the LCD glass */
    LCD_GLASS_Configure_GPIO();

    LCD_GLASS_Init();
    /* Display Welcome message */
    LCD_GLASS_Clear();
    LCD_GLASS_DisplayString( (unsigned char*)strDisp );

    LCD_GLASS_ScrollSentence(" Open32L-D ", 1, SCROLL_SPEED);
    LCD_GLASS_DisplayString("152V-D");

    while (1)
    {
        //	LCD_GLASS_ScrollSentence(" Open152V-D ",1,SCROLL_SPEED);
        LCD_GLASS_DisplayString("WAVE    ");
        Delay(1000);
        LCD_GLASS_DisplayString("SHARE   ");
        Delay(1000);
    }


}








/**
 * @brief  Configures the different system clocks.
 * @param  None
 * @retval None
 */
void RCC_Configuration(void)
{

    /* Enable HSI Clock */
    RCC_HSICmd(ENABLE);

    /*!< Wait till HSI is ready */
    while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET)
    {
    }

    RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);

    RCC_MSIRangeConfig(RCC_MSIRange_6);

    RCC_HSEConfig(RCC_HSE_OFF);
    if (RCC_GetFlagStatus(RCC_FLAG_HSERDY) != RESET )
    {
        while (1)
        {
            ;
        }
    }

    /* Enable  comparator clock LCD and PWR mngt */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_LCD | RCC_APB1Periph_PWR, ENABLE);

    /* Enable ADC clock & SYSCFG */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 | RCC_APB2Periph_SYSCFG, ENABLE);

}



void RTC_Configuration(void)
{

/* Allow access to the RTC */
    PWR_RTCAccessCmd(ENABLE);

    /* Reset Backup Domain */
    RCC_RTCResetCmd(ENABLE);
    RCC_RTCResetCmd(DISABLE);

    /* LSE Enable */
    RCC_LSEConfig(RCC_LSE_ON);

    /* Wait till LSE is ready */
    while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
    {
    }

    RCC_RTCCLKCmd(ENABLE);

    /* LCD Clock Source Selection */
    RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

}



void  Init_GPIOs(void)
{
    ;

}





/**
 * @brief  Inserts a delay time.
 * @param  nTime: specifies the delay time length, in 10 ms.
 * @retval None
 */
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}


#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}

#endif

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
