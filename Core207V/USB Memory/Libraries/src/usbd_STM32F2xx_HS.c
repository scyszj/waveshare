/*----------------------------------------------------------------------------
 *      RL-ARM - USB
 *----------------------------------------------------------------------------
 *      Name:    usbd_STM32F2xx_HS.c
 *      Purpose: Hardware Layer module for ST STM32F207ZE
 *      Rev.:    V4.21
 *----------------------------------------------------------------------------
 *      This code is part of the RealView Run-Time Library.
 *      Copyright (c) 2004-2011 KEIL - An ARM Company. All rights reserved.
 *---------------------------------------------------------------------------*/

#include <RTL.h>
#include <rl_usb.h>
#include <..\..\RL\USB\INC\usb.h>
#include <stm32f2xx.h>

#define __NO_USB_LIB_C
#include "usb_config_HS.c"


#define RX_FIFO_SIZE        512
#define TX0_FIFO_SIZE       512
#define TX1_FIFO_SIZE       512
#define TX2_FIFO_SIZE       512
#define TX3_FIFO_SIZE       512
#define TX4_FIFO_SIZE       512

#define TX_FIFO(n)          *((U32*)(OTG_HS_BASE + 0x1000 + n*0x1000))
#define RX_FIFO             *((U32*)(OTG_HS_BASE + 0x1000))

#define DIEPTSIZ(EPNum)     *(&OTG_HS->DIEPTSIZ0 + EPNum * 8)
#define DIEPCTL(EPNum)      *(&OTG_HS->DIEPCTL0  + EPNum * 8)
#define DTXFSTS(EPNum)      *(&OTG_HS->DTXFSTS0  + EPNum * 8)
#define DTXFSTS(EPNum)      *(&OTG_HS->DTXFSTS0  + EPNum * 8)
#define DOEPTSIZ(EPNum)     *(&OTG_HS->DOEPTSIZ0 + EPNum * 8)
#define DOEPCTL(EPNum)      *(&OTG_HS->DOEPCTL0  + EPNum * 8)
#define DIEPINT(EPNum)      *(&OTG_HS->DIEPINT0  + EPNum * 8)
#define DOEPINT(EPNum)      *(&OTG_HS->DOEPINT0  + EPNum * 8)


U32 TxFifoSize[5] = {TX0_FIFO_SIZE, TX1_FIFO_SIZE, TX2_FIFO_SIZE, 
                     TX3_FIFO_SIZE, TX4_FIFO_SIZE};
U32 OutMaxPacketSize[5] = {USBD_MAX_PACKET0, 0, 0, 0, 0};


/*
 *  usbd_stm32_delay
 *    Parameters:      delay:      Delay
 *    Return Value:    None
 */
void usbd_stm32_delay (U32 delay) {
  delay *= SystemCoreClock / 100000;
  while (delay--) {
    __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); __nop();
  }
}


/*
 *  USB Device Interrupt enable
 *   Called by USBD_Init to enable the USB Interrupt
 *    Return Value:    None
 */

#ifdef __RTX
void __svc(1) USBD_IntrEna (void);
void __SVC_1               (void) {
#else
void          USBD_IntrEna (void) {
#endif
  NVIC_EnableIRQ   (OTG_HS_IRQn);       /* Enable OTG interrupt               */
}


/*
 *  USB Device Initialize Function
 *   Called by the User to initialize USB
 *   Return Value:    None
 */

void USBD_Init (void) {

  RCC->AHB1ENR    |=  1 | (1UL << 1) | (1UL << 2) | (1UL << 7) | (1UL << 8);

/* UPLI data pins                                                             */
/* PA3 (OTG_HS_ULPI alternate function, DATA0)                                */
  GPIOA->MODER    =   (GPIOA->MODER  & ~(3UL  <<  6)) | (2UL  <<  6);
  GPIOA->OTYPER  &=  ~(1UL  <<  3);
  GPIOA->OSPEEDR |=   (3UL  <<  6);
  GPIOA->PUPDR   &=  ~(3UL  <<  6);
  GPIOA->AFR[0]   =   (GPIOA->AFR[0] & ~(15UL << 12)) | (10UL << 12);

/* PB0, PB1 (OTG_HS_ULPI alternate function, DATA1, DATA2)                    */
  GPIOB->MODER    =   (GPIOB->MODER  & ~(15UL <<  0)) | (10UL <<  0);
  GPIOB->OTYPER  &=  ~(3UL  <<  0);
  GPIOB->OSPEEDR |=   (15UL <<  0);
  GPIOB->PUPDR   &=  ~(15UL <<  0);
  GPIOB->AFR[0]   =   (GPIOB->AFR[0] & ~(0xFF <<  0)) | (0xAA <<  0);

/* PB10..13 (OTG_HS_ULPI alternate function, DATA3 to DATA6)                  */
  GPIOB->MODER    =   (GPIOB->MODER  & ~(0xFF << 20)) | (0xAA << 20);
  GPIOB->OTYPER  &=  ~(15UL << 10);
  GPIOB->OSPEEDR |=   (0xFF << 20);
  GPIOB->PUPDR   &=  ~(0xFF << 20);
  GPIOB->AFR[1]   =   (GPIOB->AFR[1] & ~(0xFFFF << 8)) | (0xAAAA << 8);

/* PB5 (OTG_HS_ULPI alternate function, DATA7)                                */
  GPIOB->MODER    =   (GPIOB->MODER  & ~(3UL  <<  10)) | (2UL  <<  10);
  GPIOB->OTYPER  &=  ~(1UL  <<  5);
  GPIOB->OSPEEDR |=   (3UL  << 10);
  GPIOB->PUPDR   &=  ~(3UL  << 10);
  GPIOB->AFR[0]   =   (GPIOB->AFR[0] & ~(15UL <<  20)) | (10UL <<  20);

/* ULPI control pins                                                          */
/* PC0 (OTG_HS_ULPI alternate function, STP)                                  */
  GPIOC->MODER    =   (GPIOC->MODER  & ~(3UL  <<   0)) | (2UL  <<  0);
  GPIOC->OSPEEDR |=   (3UL  <<  0);
  GPIOC->AFR[0]   =   (GPIOC->AFR[0] & ~(15UL <<   0)) | (10UL <<  0);

/*PI11 (OTG_HS_ULPI alternate functon, DIR)                                   */
  GPIOI->MODER    =   (GPIOI->MODER  & ~(3UL  <<  22)) | (2UL  << 22);
  GPIOI->OSPEEDR |=   (3UL  << 22);
  GPIOI->AFR[1]   =   (GPIOI->AFR[1] & ~(15UL <<  12)) | (10UL << 12);

/* PH4 (OTG_HS_ULPI alternate function, NXT)                                  */
  GPIOH->MODER    =   (GPIOH->MODER  & ~(3UL  <<   8)) | (2UL  <<  8);
  GPIOH->OSPEEDR |=   (3UL  <<  8);
  GPIOH->AFR[0]   =   (GPIOH->AFR[0] & ~(15UL <<  16)) | (10UL << 16);

/* PA5 (OTG_HS_ULPI a;ternate function, CLOCK)                                */
  GPIOA->MODER    =   (GPIOA->MODER  & ~(3UL  <<  10)) | (2UL  << 10);
  GPIOA->OSPEEDR |=   (3UL  << 10);
  GPIOA->AFR[0]   =   (GPIOA->AFR[0] & ~(15UL <<  20)) | (10UL << 20);

  
  RCC->AHB1ENR    |=  (3UL << 29);      /* enable clk for OTGHS and OTGHS_ULPI*/
  usbd_stm32_delay    (100);      
  RCC->AHB1RSTR   |=  (1UL << 29);      /* reset OTGHS clock                  */
  usbd_stm32_delay    (100);
  RCC->AHB1RSTR   &= ~(1UL << 29);
  usbd_stm32_delay    (400);

  OTG_HS->GRSTCTL |=  1;                /*reset otg core                      */
  OTG_HS->GUSBCFG  = (1UL << 30);       /* force device mode                  */

  USBD_IntrEna();                       /* Enable OTG interrupt               */

  OTG_HS->GAHBCFG |= 1 | (1 << 7);
  OTG_HS->GUSBCFG  = (OTG_HS->GUSBCFG & ~((15UL << 10) | (1UL << 15))) |(9 << 10);

  OTG_HS->DCFG    &= ~3;                /* HIGH speed phy                     */

  OTG_HS->GINTMSK = (1UL << 11) |       /* suspend int unmask                 */
                    (1UL << 12) |       /* reset int unmask                   */
                    (1UL << 13) |       /* enumeration done int unmask        */
                    (1UL << 4 ) |       /* recive fifo non-empty int  unmask  */
                    (1UL << 18) |       /* IN EP int unmask                   */
                    (1UL << 31) |       /* resume int unmask                  */
#ifdef __RTX
  ((USBD_RTX_DevTask   != 0) ? (1UL <<  3) : 0);   /* SOF int unmask          */
#else
  ((USBD_P_SOF_Event   != 0) ? (1UL <<  3) : 0);   /* SOF int unmask          */
#endif
}




/*
 *  USB Device Connect Function
 *   Called by the User to Connect/Disconnect USB Device
 *    Parameters:      con:   Connect/Disconnect
 *    Return Value:    None
 */

void USBD_Connect (BOOL con) {

  if (con) {
    OTG_HS->GCCFG   =   (1UL << 19) |   /* enable Vbus*                       */
                        (1UL << 16);    /* power down deactivated             */
    OTG_HS->DCTL   &=  ~(1UL << 1 );    /* soft disconnect disabled           */

  }
  else {
    OTG_HS->GCCFG  &= ~(1UL << 19);     /* disable Vbus*                      */
    OTG_HS->DCTL   |=  (1UL << 1 );     /* soft disconnect enabled            */
  }
}


/*
 *  USB Device Reset Function
 *   Called automatically on USB Device Reset
 *    Return Value:    None
 */

void USBD_Reset (void) {
  U32 i;

  for (i = 0; i < (USBD_EP_NUM + 1); i++) {
   if (DOEPCTL(i) & (1UL << 31))
     DOEPCTL(i) = (1UL << 30) | (1UL << 27); /* OUT EP disable, Set NAK       */
   if (DIEPCTL(i) & (1UL << 31))
     DIEPCTL(i) = (1UL << 30) | (1UL << 27); /* IN EP disable, Set NAK        */
  }

  USBD_SetAddress(0 , 1);

  OTG_HS->DAINTMSK = 1 | (1UL << 16);   /* unmask IN&OUT EP0 interruts        */

  OTG_HS->DOEPMSK  =  (1UL << 3) |      /* setup phase done                   */
                       1;               /*transfer complete                   */
  OTG_HS->DIEPMSK  =   1;               /*transfer completed                  */

  OTG_HS->GRXFSIZ  =  RX_FIFO_SIZE;
  OTG_HS->TX0FSIZ  =  (TX0_FIFO_SIZE << 16) | RX_FIFO_SIZE;

  OTG_HS->DIEPTXF1 = (RX_FIFO_SIZE + TX0_FIFO_SIZE) |
                     (TX1_FIFO_SIZE << 16);
  OTG_HS->DIEPTXF2 = (RX_FIFO_SIZE + TX0_FIFO_SIZE + TX1_FIFO_SIZE) |
                     (TX2_FIFO_SIZE << 16);
  OTG_HS->DIEPTXF3 = (RX_FIFO_SIZE + TX0_FIFO_SIZE + TX1_FIFO_SIZE +TX2_FIFO_SIZE)|
                     (TX3_FIFO_SIZE << 16);
  OTG_HS->DIEPTXF4 = (RX_FIFO_SIZE + TX0_FIFO_SIZE + TX1_FIFO_SIZE +
                      TX2_FIFO_SIZE + TX3_FIFO_SIZE) |(TX4_FIFO_SIZE << 16);

  OTG_HS->DOEPTSIZ0   =  (3UL << 29) |  /* setup count = 3                    */
                         (1UL << 19) |  /* packet count                       */
                          USBD_MAX_PACKET0;
}


/*
 *  USB Device Suspend Function
 *   Called automatically on USB Device Suspend
 *    Return Value:    None
 */

void USBD_Suspend (void) {
}


/*
 *  USB Device Resume Function
 *   Called automatically on USB Device Resume
 *    Return Value:    None
 */

void USBD_Resume (void) {
}


/*
 *  USB Device Remote Wakeup Function
 *   Called automatically on USB Device Remote Wakeup
 *    Return Value:    None
 */

void USBD_WakeUp (void) {
  OTG_HS->DCTL |= 1;                    /* remote wakeup signaling            */
  usbd_stm32_delay (50);                /* Wait ~5 ms                         */
  OTG_HS->DCTL &= ~1;
}


/*
 *  USB Device Remote Wakeup Configuration Function
 *    Parameters:      cfg:   Device Enable/Disable
 *    Return Value:    None
 */

void USBD_WakeUpCfg (BOOL cfg) {
  /* Not needed                                                               */
}


/*
 *  USB Device Set Address Function
 *    Parameters:      adr:   USB Device Address
 *    Return Value:    None
 */

void USBD_SetAddress (U32  adr, U32 setup) {
  if (setup) {
    OTG_HS->DCFG = (OTG_HS->DCFG & ~(0x7f << 4)) | (adr << 4);
  }
}


/*
 *  USB Device Configure Function
 *    Parameters:      cfg:   Device Configure/Deconfigure
 *    Return Value:    None
 */

void USBD_Configure (BOOL cfg) {
}


/*
 *  Configure USB Device Endpoint according to Descriptor
 *    Parameters:      pEPD:  Pointer to Device Endpoint Descriptor
 *    Return Value:    None
 */

void USBD_ConfigEP (USB_ENDPOINT_DESCRIPTOR *pEPD) {
  U32 num, val, type;

  num  = pEPD->bEndpointAddress & ~(0x80);
  val  = pEPD->wMaxPacketSize;
  type = pEPD->bmAttributes & USB_ENDPOINT_TYPE_MASK;

  if (pEPD->bEndpointAddress & USB_ENDPOINT_DIRECTION_MASK) {
    OTG_HS->DAINTMSK |= (1UL  << num);  /* unmask IN EP int                   */
    DIEPCTL(num)      = (num  <<  22) | /* fifo number                        */
                        (type <<  18) | /* ep type                            */
                         val;           /* max packet size                    */
    if (((DIEPCTL(num) >> 18) & 3) > 1) /* if interrupt or bulk EP            */
      DIEPCTL(num) |= (1 << 28);    
  }
  else {
    OutMaxPacketSize[num] = val;
    OTG_HS->DAINTMSK     |= (1UL  <<    num) << 16; /* unmask IN EP int       */
    DOEPCTL(num)          = (type <<     18) |      /* EP type                */
                            (val  &   0x7FF);       /* max packet size        */
    DOEPTSIZ(num)         = (1UL  <<     19) |      /* packet count = 1       */
                            (val  & 0x7FFFF);       /* transfer size          */
    if (((DOEPCTL(num) >> 18) & 3) > 1)             /* if int or bulk EP      */
      DOEPCTL(num)       |= (1 << 28);
  }
}


/*
 *  Set Direction for USB Device Control Endpoint
 *    Parameters:      dir:   Out (dir == 0), In (dir <> 0)
 *    Return Value:    None
 */

void USBD_DirCtrlEP (U32 dir) {
  /* Not needed                                                               */
}


/*
 *  Enable USB Device Endpoint
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_EnableEP (U32 EPNum) {
  if (EPNum & 0x80) {
    EPNum &= ~0x80;
    DIEPCTL(EPNum)    |= (1UL << 15) |  /* EP active                          */
                         (1UL << 27);   /* set EP NAK                         */
    if (DIEPCTL(EPNum)&  (1UL << 31))
      DIEPCTL(EPNum)  |= (1UL << 30);   /* disable EP                         */
  }
  else {
    DOEPCTL(EPNum)    |= (1UL << 15) |  /* EP active                          */
                         (1UL << 31) |  /* enable EP                          */
                         (1UL << 26);   /* clear EP NAK                       */
  }
}


/*
 *  Disable USB Endpoint
 *    Parameters:      EPNum: Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_DisableEP (U32 EPNum) {
  if (EPNum & 0x80) {
    EPNum &= ~0x80;
    if (DIEPCTL(EPNum) &   (1UL << 31))
      DIEPCTL(EPNum)   |=  (1UL << 30); /* disable EP                         */
    DIEPCTL(EPNum)     |=  (1UL << 27); /* set EP NAK                         */
    DIEPCTL(EPNum)     &= ~(1UL << 15); /* deactivate EP                      */
  }
  else {
    if (DOEPCTL(EPNum) &   (1UL << 31))
      DOEPCTL(EPNum)   |=  (1UL << 30); /* disable EP                         */
    DOEPCTL(EPNum)     |=  (1UL << 27); /* set EP NAK                         */
    DOEPCTL(EPNum)     &= ~(1UL << 15); /* deactivate EP                      */
  }
}


/*
 *  Reset USB Device Endpoint
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_ResetEP (U32 EPNum) {
  if (EPNum & 0x80) {
    EPNum &= ~0x80;
    if (DIEPCTL(EPNum) &  (1UL << 31))
      DIEPCTL(EPNum)   |= (1UL << 30);  /* disable EP                         */
    DIEPCTL(EPNum)     |= (1UL << 27);  /* set EP NAK                         */

    OTG_HS->GRSTCTL = (OTG_HS->GRSTCTL & ~(0x1F << 6)) | /* flush EP fifo     */
                      (EPNum << 6)| (1UL << 5);
    while (OTG_HS->GRSTCTL & (1UL << 5));
  }
}

/*
 *  Set Stall for USB Device Endpoint
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_SetStallEP (U32 EPNum) {
  if (!(EPNum & 0x80)) {
    DOEPCTL(EPNum)     |= (1UL << 21);  /*set stall                           */
  }
  else {
    EPNum &= ~0x80;
    if (DIEPCTL(EPNum) &  (1UL << 31)) {
      DIEPCTL(EPNum)   |= (1UL << 30);
    }
    DIEPCTL(EPNum)     |= (1UL << 21);  /*set stall                           */

    OTG_HS->GRSTCTL     = (OTG_HS->GRSTCTL & ~(0x1f << 6)) | /* flush EP fifo */
                          (EPNum << 6)| (1UL << 5);

    while (OTG_HS->GRSTCTL & (1UL << 5));
  }
}


/*
 *  Clear Stall for USB Device Endpoint
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_ClrStallEP (U32 EPNum) {
  if (!(EPNum & 0x80)) {
    if (((DOEPCTL(EPNum) >> 18) & 3) > 1)/* if interrupt or bulk EP           */
      DOEPCTL(EPNum)    =  (DOEPCTL(EPNum) & ~(1 << 21)) |  (1 << 28);
    else
      DOEPCTL(EPNum)   &= ~(1UL << 21); /* clear stall                        */
  }
  else {
    EPNum &= ~0x80;
    if (DIEPCTL(EPNum) &  (1UL << 31))
      DIEPCTL(EPNum)   |= (1UL << 30);
    DIEPCTL(EPNum)     |= (1UL << 27);

    OTG_HS->GRSTCTL     = (OTG_HS->GRSTCTL & ~(0x1f << 6)) | /* flush EP fifo */
                          (EPNum << 6)| (1UL << 5);
    while (OTG_HS->GRSTCTL & (1UL << 5));

    if (((DIEPCTL(EPNum) >> 18) & 3) > 1)/* if interrupt or bulk EP           */
      DIEPCTL(EPNum)    =   (DIEPCTL(EPNum) & ~(1 << 21)) |  (1 << 28);
    else
      DIEPCTL(EPNum)   &= ~(1UL << 21); /* clear stall                        */
  }
}


/*
 *  Clear USB Device Endpoint Buffer
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *    Return Value:    None
 */

void USBD_ClearEPBuf (U32 EPNum) {
  if (EPNum & 0x80) {
    EPNum &= ~0x80;
    OTG_HS->GRSTCTL = (OTG_HS->GRSTCTL & ~(0x1f << 6)) | /* flush EP fifo     */
                      (EPNum << 6)| (1UL << 5);
    while (OTG_HS->GRSTCTL & (1UL << 5));
  }
}


/*
 *  Read USB Device Endpoint Data
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *                     pData: Pointer to Data Buffer
 *    Return Value:    Number of bytes read
 */

U32 USBD_ReadEP (U32 EPNum, U8 *pData) {
  U32 i, sz;

  sz = ( OTG_HS->GRXSTSP >> 4) & 0x7FF;

  for (i = 0; i < (U32)((sz+3)/4); i++) {
    *((__packed U32 *)pData) = RX_FIFO;
    pData += 4;
  }

  DOEPTSIZ(EPNum)  =  (1UL << 19) |                  /* packet count          */
                      (OutMaxPacketSize[EPNum]    ); /* transfer size         */
  if (EPNum == 0) {
    DOEPTSIZ(0)   |= (3UL << 29);
  }
  DOEPCTL(EPNum)  |= (1UL <<31) | (1UL << 26);       /* clear NAK, enable EP  */
  OTG_HS->GINTMSK |= (1UL << 4);

  return (sz);
}


/*
 *  Write USB Device Endpoint Data
 *    Parameters:      EPNum: Device Endpoint Number
 *                       EPNum.0..3: Address
 *                       EPNum.7:    Dir
 *                     pData: Pointer to Data Buffer
 *                     cnt:   Number of bytes to write
 *    Return Value:    Number of bytes written
 */

U32 USBD_WriteEP (U32 EPNum, U8 *pData, U32 cnt) {
  U32 i;

  EPNum &= ~(0x80);

  if (cnt) {
    while (DTXFSTS(EPNum) < cnt);

    DIEPTSIZ(EPNum) = (DIEPTSIZ(EPNum) & ~0x1FFFFFFF) | cnt | (1UL << 19);
    DIEPCTL(EPNum) |= (1UL << 31) | (1UL << 26);

    for (i = 0; i < (cnt+3)/4; i++) {
      TX_FIFO(EPNum) = *((__packed U32 *)pData);
      pData +=4;
    }
  }
  else {
    DIEPTSIZ(EPNum) = (DIEPTSIZ(EPNum) & ~0x1FFFFFFF)| (1<< 19);
    DIEPCTL(EPNum) |= (1UL << 31) | (1UL << 26);
  }

  return (cnt);
}


/*
 *  Get USB Device Last Frame Number
 *    Parameters:      None
 *    Return Value:    Frame Number
 */

U32 USBD_GetFrame (void) {
  return ((OTG_HS->DSTS >> 8) & 0x3FFF);
}


/*
 *  USB Device Interrupt Service Routine
 */
void OTG_HS_IRQHandler(void) {
  U32 istr, val, num, i;

  istr = OTG_HS->GINTSTS & OTG_HS->GINTMSK;

/* reset interrupt                                                            */
  if (istr & (1UL << 12)) {
    USBD_Reset();
    usbd_reset_core();
#ifdef __RTX
    if (USBD_RTX_DevTask) {
      isr_evt_set(USBD_EVT_RESET, USBD_RTX_DevTask);
    }
#else
    if (USBD_P_Reset_Event) {
      USBD_P_Reset_Event();
    }
#endif
    OTG_HS->GINTSTS |= (1UL << 12);
  }

/* suspend interrupt                                                          */
  if (istr & (1UL << 11)) {
    USBD_Suspend();
#ifdef __RTX
    if (USBD_RTX_DevTask) {
      isr_evt_set(USBD_EVT_SUSPEND, USBD_RTX_DevTask);
    }
#else
    if (USBD_P_Suspend_Event) {
      USBD_P_Suspend_Event();
    }
#endif
    OTG_HS->GINTSTS |= (1UL << 11);
  }

/* resume interrupt                                                           */
  if (istr & (1UL << 31)) {
    USBD_Resume();
#ifdef __RTX
    if (USBD_RTX_DevTask) {
      isr_evt_set(USBD_EVT_RESUME, USBD_RTX_DevTask);
    }
#else
    if (USBD_P_Resume_Event) {
      USBD_P_Resume_Event();
    }
#endif
    OTG_HS->GINTSTS |= (1UL << 31);
  }

/* speed enumeration completed                                                */
  if (istr & (1UL << 13)) {
    if (!((OTG_HS->DSTS >> 1) & 3)) {
      USBD_HighSpeed = __TRUE;
    }
    OTG_HS->DIEPCTL0 |= OutMaxPacketSize[0] ;   /* EP0 max packet             */
    OTG_HS->DCTL     |= (1UL << 8);     /* clear global IN NAK                */
    OTG_HS->DCTL     |= (1UL << 10);    /* clear global OUT NAK               */
    OTG_HS->GINTSTS  |= (1UL << 13);
  }

/* Start Of Frame                                                             */
  if (istr & (1UL << 3)) {
#ifdef __RTX
    if (USBD_RTX_DevTask) {
      isr_evt_set(USBD_EVT_SOF, USBD_RTX_DevTask);
    }
#else
    if (USBD_P_SOF_Event) {
      USBD_P_SOF_Event();
    }
#endif
     OTG_HS->GINTSTS |= (1UL << 3);
  }

/* RxFIFO non-empty                                                           */
  if (istr & (1UL << 4)) {
    val = OTG_HS->GRXSTSR;
    num = val & 0x0F;
/* setup packet                                                               */
    if (((val >> 17) & 0x0F) == 6) {
      OTG_HS->GINTMSK &= ~(1UL << 4);
#ifdef __RTX
      if (USBD_RTX_EPTask[num]) {
        isr_evt_set(USBD_EVT_SETUP, USBD_RTX_EPTask[num]);
      }
#else
      if (USBD_P_EP[num]) {
        USBD_P_EP[num](USBD_EVT_SETUP);
      }
#endif
    }
/* OUT packet                                                                 */
    else if (((val >> 17) & 0x0F) == 2) { /* data out packed received         */
      OTG_HS->GINTMSK &= ~(1UL << 4);
#ifdef __RTX
      if (USBD_RTX_EPTask[num]) {
        isr_evt_set(USBD_EVT_OUT, USBD_RTX_EPTask[num]);
      }
#else
      if (USBD_P_EP[num]) {
        USBD_P_EP[num](USBD_EVT_OUT);
      }
#endif
    }
    else {
      OTG_HS->GRXSTSP;
    }
  }

/* IN Packet                                                                  */
  if (istr & (1UL << 18)) {
    num = (OTG_HS->DAINT & OTG_HS->DAINTMSK & 0xFFFF);
    for (i = 0; i < (USBD_EP_NUM+1); i++) {
      if ((num >> i) & 1) {
        num = i;
        break;
      }
    }
    if (DIEPINT(num) & 1) {             /* TxFIFO compleated                  */
#ifdef __RTX
      if (USBD_RTX_EPTask[num]) {
        isr_evt_set(USBD_EVT_IN,  USBD_RTX_EPTask[num]);
      }
#else
      if (USBD_P_EP[num]) {
        USBD_P_EP[num](USBD_EVT_IN);
      }
#endif
      DIEPINT(num) |= 1;
    }
  }
}
