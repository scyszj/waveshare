#include "stm32l1xx.h"
#include "24C02.h"
#include <stdio.h>
#include <string.h>
    #define DEBUG_SWD_PIN
USART_InitTypeDef USART_InitStructure;
/* Private function prototypes -----------------------------------------------*/
    #ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
    #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
    #else
    #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE * f)
    #endif /* __GNUC__ */


RCC_ClocksTypeDef RCC_Clocks;
static volatile uint32_t TimingDelay;
void RTC_Configuration(void);
void RCC_Configuration(void);
void Delay(uint32_t nTime);
void USART_Configuration(void);
void GPIO_Configuration(void);
int main(void)
{
    uint16_t Addr;
    uint8_t WriteBuffer[256], ReadBuffer[256];

//	/* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500祍 */
//	RCC_GetClocksFreq(&RCC_Clocks);
//	SysTick_Config(RCC_Clocks.HCLK_Frequency /100);
//
    USART_Configuration();

    GPIO_Configuration();

    I2C_Configuration();


    for (Addr = 0; Addr < 256; Addr++)
    {
        WriteBuffer[Addr] = Addr;  /* 填充WriteBuffer */

    }
    /* 开始向EEPROM写数据 */
    printf("\r\n EEPROM 24C02 Write Test \r\n");
    I2C_Write(I2C1, ADDR_24LC02, 0, WriteBuffer, sizeof(WriteBuffer) );
    printf("\r\n EEPROM 24C02 Write Test OK \r\n");

    /* EEPROM读数据 */
    printf("\r\n EEPROM 24C02 Read Test \r\n");
    I2C_Read(I2C1, ADDR_24LC02, 0, ReadBuffer, sizeof(WriteBuffer) );

    if (  memcmp(WriteBuffer, ReadBuffer, sizeof(WriteBuffer)) == 0 ) /* 匹配数据 */
    {
        printf("\r\n EEPROM 24C02 Read Test OK\r\n");
    }
    else
    {
        printf("\r\n EEPROM 24C02 Read Test False\r\n");
    }

    /* Infinite loop */
    while (1)
    {
        printf("\r\n EEPROM 24C02 Read Test OK\r\n");
        Delay(100);
    }

}

void GPIO_Configuration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE);
/**
 *  LED1 -> PB15 , LED2 -> PB14 , LED3 -> PB13 , LED4 -> PB12
 */
    GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
}

/* USARTx configured as follow:
      - BaudRate = 115200 baud
      - Word Length = 8 Bits
      - One Stop Bit
      - No parity
      - Hardware flow control disabled (RTS and CTS signals)
      - Receive and transmit enabled
 */

void USART_Configuration(void )
{


    GPIO_InitTypeDef GPIO_InitStructure;

    /* Enable GPIO clock */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

    /* Enable USART1 clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    /* Connect PXx to USARTx_Tx */
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);

    /* Connect PXx to USARTx_Rx */
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

    /* Configure USART Tx as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* Configure USART Rx as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* USART configuration */
    USART_Init(USART1, &USART_InitStructure);

    /* Enable USART */
    USART_Cmd(USART1, ENABLE);
}






/**
 * @brief  Configures the different system clocks.
 * @param  None
 * @retval None
 */
void RCC_Configuration(void)
{

    /* Enable HSI Clock */
    RCC_HSICmd(ENABLE);

    /*!< Wait till HSI is ready */
    while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET)
    {
    }

    RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);

    RCC_MSIRangeConfig(RCC_MSIRange_6);

    RCC_HSEConfig(RCC_HSE_OFF);
    if (RCC_GetFlagStatus(RCC_FLAG_HSERDY) != RESET )
    {
        while (1)
        {
            ;
        }
    }


    /* Enable ADC clock & SYSCFG */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

}
/**
 * @brief  Inserts a delay time.
 * @param  nTime: specifies the delay time length, in 10 ms.
 * @retval None
 */
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}

#endif



PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(USART1, (uint8_t)ch);

    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
    {
    }

    return ch;
}

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
