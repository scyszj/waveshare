#include "stm32l1xx.h"
#include <stdio.h>
#include "sd.h"
#include "User_systick.h"
uint8_t buffer[512];
    #define DEBUG_SWD_PIN
USART_InitTypeDef USART_InitStructure;
/* Private function prototypes -----------------------------------------------*/
    #ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
    #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
    #else
    #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE * f)
    #endif /* __GNUC__ */


RCC_ClocksTypeDef RCC_Clocks;
static volatile uint32_t TimingDelay;
void RTC_Configuration(void);
void RCC_Configuration(void);
void Delay(uint32_t nTime);
void USART_Configuration(void);

int main(void)
{
    RCC_ClocksTypeDef SYS_Clocks;
    uint32_t sd_size;
    uint8_t res;
    uint16_t i;

    SystemInit();
    USART_Configuration();
    RCC_GetClocksFreq(&SYS_Clocks);


    printf("Show The MCU USEING CLK:\r\n");
    printf("SYSCLK:%dM\r\n", SYS_Clocks.SYSCLK_Frequency / 1000000);
    printf("HCLK:%dM\r\n", SYS_Clocks.HCLK_Frequency / 1000000);
    printf("PCLK1:%dM\r\n", SYS_Clocks.PCLK1_Frequency / 1000000);
    printf("PCLK2:%dM\r\n", SYS_Clocks.PCLK2_Frequency / 1000000);




    printf("SD card experiment！\r\n");
    printf("Please plug in 1 GB SD card！\r\n");
    while (SD_Init() != 0) //循环检测SD卡是否存在 (Cycle detection SD card was there)
    {
        printf("No SD card detected！\r\n");
        delay_1ms(500);
        printf("lease confirm whether inserted SD card\r\n");
        delay_1ms(500);
    }
    printf("SD card is OK\r\n");
    sd_size = SD_GetCapacity();
    sd_size = (sd_size >> 20);
    printf("SD card size capacity:%d Mb\r\n", sd_size);

    /*------     给SD卡写入数据  SD card to write data   ------*/
    for (i = 0; i < 512; i++) // 给buf赋值(Buf to assign a value to)
    {
        buffer[i] = 'a';     // 写入512个字母'a' (Written 512 letters' a ')
    }
    res = SD_WriteSingleBlock(10, buffer); // 把buf的数据写入到SD卡的第10个扇区(Buf data written to the SD card of 10 sector)
    if (res == 0)   // 如果写入成功   (If written to the success)
    {
        printf("Written to the success！\r\n");
    }
    else         // 如果写入失败 (If written to the failure)
    {
        printf("Written to the failure！\r\n");
    }

    /*------     读SD卡的数据    Read the SD card data ------*/
    for (i = 0; i < 512; i++) // 给buf赋值	  Buf to assign a value to
    {
        buffer[i] = 'b';     // 把buf的值重新赋值为‘b’ (The value of the buf to value of "b")
    }
    res = SD_ReadSingleBlock(10, buffer); // 读SD卡的第10个扇区，并把读出值存入buf(Read the SD card sector 10, and read the value buf deposit)
    if (res == 0)   // 如果读出数据成功 (If read data success)
    {
        printf("Read success！\r\n");
    }
    else         // 如果读出数据失败  (If read data failed)
    {
        printf("Read data failed！\r\n");
    }

    if (res == 0) // 如果读出数据成功(If read data success)
    {
        for (i = 0; i < 512; i++)
        {
            if (buffer[i] != 'a') // 检测每一个读到的数据是否为a(Testing every read data whether for a)
            {
                break;         // 只要有一个数据不是a，就提前跳出for循环(As long as there is a data is not a, jumped out in advance for circulation)
            }
        }
        if (i == 512) // 如果i=512,即所有数据全正确 (If i = 512, that all the right data)
        {
            printf("Testing is complete！All the data are correct！\r\n");
        }
        else       // 如果i!=512,即读到的数据发生错误(If i! = 512, or read data error)
        {
            printf("Testing is complete！Read the wrong data！\r\n");
        }
    }

    while (1)
    {
        ;
    }

}


/* USARTx configured as follow:
      - BaudRate = 115200 baud
      - Word Length = 8 Bits
      - One Stop Bit
      - No parity
      - Hardware flow control disabled (RTS and CTS signals)
      - Receive and transmit enabled
 */

void USART_Configuration(void )
{


    GPIO_InitTypeDef GPIO_InitStructure;

    /* Enable GPIO clock */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

    /* Enable USART1 clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

    USART_InitStructure.USART_BaudRate = 115200;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    /* Connect PXx to USARTx_Tx */
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);

    /* Connect PXx to USARTx_Rx */
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

    /* Configure USART Tx as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* Configure USART Rx as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    /* USART configuration */
    USART_Init(USART1, &USART_InitStructure);

    /* Enable USART */
    USART_Cmd(USART1, ENABLE);
}






/**
 * @brief  Configures the different system clocks.
 * @param  None
 * @retval None
 */
void RCC_Configuration(void)
{

    /* Enable HSI Clock */
    RCC_HSICmd(ENABLE);

    /*!< Wait till HSI is ready */
    while (RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET)
    {
    }

    RCC_SYSCLKConfig(RCC_SYSCLKSource_HSI);

    RCC_MSIRangeConfig(RCC_MSIRange_6);

    RCC_HSEConfig(RCC_HSE_OFF);
    if (RCC_GetFlagStatus(RCC_FLAG_HSERDY) != RESET )
    {
        while (1)
        {
            ;
        }
    }


    /* Enable ADC clock & SYSCFG */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

}
/**
 * @brief  Inserts a delay time.
 * @param  nTime: specifies the delay time length, in 10 ms.
 * @retval None
 */
//void Delay(uint32_t nTime)
//{
//  TimingDelay = nTime;
//
//  while(TimingDelay != 0);
//
//}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
//void TimingDelay_Decrement(void)
//{
//
//  if (TimingDelay != 0x00)
//  {
//    TimingDelay--;
//  }
//
//}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}

#endif



PUTCHAR_PROTOTYPE
{
    /* Place your implementation of fputc here */
    /* e.g. write a character to the USART */
    USART_SendData(USART1, (uint8_t)ch);

    /* Loop until the end of transmission */
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET)
    {
    }

    return ch;
}

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
