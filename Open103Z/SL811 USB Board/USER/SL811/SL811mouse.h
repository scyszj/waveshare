/********************************************************************************************************
*
* File                : SL811mouse.h
* Hardware Environment: 
* Build Environment   : RealView MDK-ARM  Version: 4.14
* Version             : V1.0
* By                  : xiao xian hui
*
*                                  (c) Copyright 2005-2011, WaveShare
*                                       http://www.waveShare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

#ifndef __SL811MOUSE_H
#define __SL811MOUSE_H

/* Includes ------------------------------------------------------------------*/
#include "Host_SL811.h"
#include "stm32f10x.h"
#include <stdio.h>

/* Private typedef -----------------------------------------------------------*/
struct HidMouseStruct
{
	int button_L,button_R,button_M;
	int x;
	int y;
	int wheel;
};

/* Private function prototypes -----------------------------------------------*/
int MouseInit(void);
void MouseTask(void);
int sl811Initformouse(void);
int EnumUsbDevformouse(uint8_t usbaddr);

#endif 

/*********************************************************************************************************
      END FILE
*********************************************************************************************************/
