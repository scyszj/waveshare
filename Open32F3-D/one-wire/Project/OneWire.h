#ifndef _ONEWIRE_H
#define _ONEWIRE_H
#include "stm32f30x.h"

#define SET_DQ()        GPIO_SetBits(GPIOB, GPIO_Pin_5)
#define CLR_DQ()        GPIO_ResetBits(GPIOB, GPIO_Pin_5)

#define GET_DQ()        GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_5)

void Onewire_Enable_GPIO_Port(void);
void Onewire_OUT_PULL_UP(void);
void Onewire_OUT_FLOATING(void);
void Onewire_IN_FLOATING(void);
void Onewire_IN_PULL_UP(void);
void _delay_us(uint8_t us);
void resetOnewire(void);
uint8_t rOnewire(void);
void wOnewire(uint8_t data);

#endif /*_ONEWIRE_H*/
