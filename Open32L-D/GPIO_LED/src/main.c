#include "misc.h"

#define GPIO_HIGH(a, b)      a->BSRRL = b
#define GPIO_LOW(a, b)       a->BSRRH = b
#define GPIO_TOGGLE(a, b)    a->ODR ^= b
void  Delay(uint32_t nCount);
int main(void)
{

    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure the LED_pin as output push-pull for LD3 & LD4 usage*/
    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOB, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_6;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    /* Force a low level on LEDs*/
    GPIO_ResetBits(GPIOB, GPIO_Pin_7);
    GPIO_ResetBits(GPIOB, GPIO_Pin_6);

    while (1)
    {
        GPIO_ResetBits(GPIOB, GPIO_Pin_7);
        Delay(0xfffff);
        GPIO_SetBits(GPIOB, GPIO_Pin_6);
        Delay(0xfffff);
        GPIO_SetBits(GPIOB, GPIO_Pin_7);
        Delay(0xfffff);
        GPIO_ResetBits(GPIOB, GPIO_Pin_6);
        Delay(0xfffff);

    }
}

void  Delay(uint32_t nCount)
{
    for (; nCount != 0; nCount--)
    {
        ;
    }
}
