/**
 ******************************************************************************
 * @file    Humidity_Measure.c
 * @author  MCD Application Team
 * @version V1.0.0
 * @date    13-November-2012
 * @brief   This file includes the Humidity measure driver for the STM32303C-EVAL
 *          demonstration.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stdio.h>

/** @addtogroup STM32F30x_StdPeriph_Examples
 * @{
 */

/** @addtogroup Humidity_measure
 * @{
 */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern __IO uint16_t AvrgICReadValue;
uint16_t ICError = 0;
uint8_t ExitMenu = 0;
uint8_t DisplayValue = 50;
uint16_t PeriodValue = 0;
uint8_t HumidityMeas = 0;

__IO float RelativeHumidity = 0.0f;
float Capacitance = 0.0f;
float TriggerTime = 0.0f;
float Capacitance55RH = 0.00000000018f;
float CapacitanceRatio = 0.0f;
char LCDstr[20];

/* Private function prototypes -----------------------------------------------*/
static void TIM3_PWM_Config(void);
static void COMP4_Config(void);
static void TIM4_IC_Config(void);


/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Humidity_measure program.
 * @param  None
 * @retval None
 */
void Menu_Humidity_MeasureFunc(void)
{


    HumidityMeas = 1;


    /* Enable the PWR clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

    /* Allow access to RTC */
    PWR_BackupAccessCmd(ENABLE);

    /* Comparator Configuration */
    COMP4_Config();

    /* TIM3 channel3 Configuration in PWM Mode */
    TIM3_PWM_Config();

    /* TIM4 Channel2 Configuration in Input Capture Mode */
    TIM4_IC_Config();

    /* Enable TIM3 (TIM4 is enabled in the same cycle) */
    TIM_Cmd(TIM3, ENABLE);

    /* ICError initialisation */
#ifdef USE_DAC
    ICError = (uint16_t)(DEFAULT_OFFSET_DAC * (float)SystemCoreClock / 72000000);
#elif USE_VREFINT
    ICError = (uint16_t)(DEFAULT_OFFSET_VRI * (float)SystemCoreClock / 72000000);
#endif /* USE_DAC */


    /* wait until first AvrgICReadValue is calculated */
    while (AvrgICReadValue == 0)
    {
        ;
    }

    if (RTC_ReadBackupRegister(RTC_BKP_DR14) != 0x5AA5)
    {
        /* Write 0x5AA5 to indicate that HUM is calibrated */
        RTC_WriteBackupRegister(RTC_BKP_DR14, 0x5AA5);

        /* Enter Calibration menu if Select Button Pressed During Startup */
//    Calibration_Menu();

        RTC_WriteBackupRegister(RTC_BKP_DR15, ICError);
    }
    else
    {
        ICError = RTC_ReadBackupRegister(RTC_BKP_DR15);
    }
    /* Infinite loop */
    while (1)
    {

        /* Calculate Trigger Time Value */
        TriggerTime = (float)(AvrgICReadValue - ICError) / SystemCoreClock;

#ifdef USE_DAC
        /* Comp4 inverted input connected to DAC1 :
         * 2.086 Reference Voltage gives a Trigger Time of R*C.
         */
        Capacitance = (float)TriggerTime / RES;

#elif USE_VREFINT
        /* Comp4 inverted input connected to VrefInt
         * 1.22V Reference Voltage gives a Trigger Time of R*C*VREFINT_FACTOR
         */
        Capacitance = (float)TriggerTime / (RES * VREFINT_FACTOR);

#endif /* USE_DAC */

        /* Calculate humidity value using reversed polynomial expression */
        CapacitanceRatio = Capacitance / Capacitance55RH;
        RelativeHumidity = RP3 * pow(CapacitanceRatio, 3) + RP2 * pow(CapacitanceRatio, 2) + RP1 * CapacitanceRatio + RP0;

        /* Restrict Relative Humidity Value to 0-99 Domain */
//       if (RelativeHumidity<0)
//       {
//         RelativeHumidity=0;
//       }
//       if (RelativeHumidity>99)
//       {
//         RelativeHumidity=99;
//       }
        printf("\n\rRelativeHumidity:%d\n\r", (uint8_t)RelativeHumidity);


    }

}

/**
 * @brief  Configures TIM3: channels in PWM mode
 * @param  None
 * @retval None
 */
static void TIM3_PWM_Config(void)
{
    TIM_OCInitTypeDef TIM_OCInitStructure;
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    GPIO_InitTypeDef GPIO_InitStructure;

#ifdef TIM3_PWM_PB0
    /* GPIOB clock enable */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

    /* TIM3 channel pin configuration: TIM3_CH3 -> PB0 */
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* Map TIM3 CH3 to PB0 */
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource0, GPIO_AF_2);

#elif TIM3_PWM_PC8
    /* GPIOC clock enable */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);

    /* TIM3 channel pin configuration: TIM3_CH3 -> PC8 */
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_8;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    /* Map TIM3 CH3 to PC8 */
    GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_2);

#elif TIM3_PWM_PE4
    /* GPIOC clock enable */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE, ENABLE);

    /* TIM3 channel pin configuration: TIM3_CH3 -> PE4 */
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_4;
    GPIO_Init(GPIOE, &GPIO_InitStructure);

    /* Map TIM3 CH3 to PE4 */
    GPIO_PinAFConfig(GPIOE, GPIO_PinSource4, GPIO_AF_2);

#endif /* TIM3_PWM Output Pin Select*/

    /* TIM3 clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    /* Calculate the period value */
    PeriodValue = (uint16_t)((SystemCoreClock) / FREQ);

    /* Time Base configuration */
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period = PeriodValue;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

    /* Channel 3 configuration in PWM mode */
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = PeriodValue * DUTY / 100;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;
    TIM_OC3Init(TIM3, &TIM_OCInitStructure);

    /* Select the Master Slave Mode to Synchronize TIM3 with TIM4 */
    TIM_SelectMasterSlaveMode(TIM3, TIM_MasterSlaveMode_Enable);
    /* Master Mode Trigger Output Selection */
    TIM_SelectOutputTrigger(TIM3, TIM_TRGOSource_Enable);
}


/**
 * @brief  Configures COMP4: PB0 as COMP4 non inverting input
 *                           VREFINT/DAC1 as COMP4 inverting input
 *                           and COMP4 output to TIM4 IC2.
 * @param  None
 * @retval None
 */
static void COMP4_Config(void)
{
    COMP_InitTypeDef COMP_InitStructure;

#ifdef USE_DAC
    DAC_InitTypeDef DAC_InitStructure;

    /* DAC clock enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_DAC, ENABLE);

    /* DAC configuration */
    DAC_InitStructure.DAC_Trigger = DAC_Trigger_None;
    DAC_InitStructure.DAC_WaveGeneration = DAC_WaveGeneration_None;
    DAC_InitStructure.DAC_LFSRUnmask_TriangleAmplitude = DAC_LFSRUnmask_Bit0;
    DAC_InitStructure.DAC_OutputBuffer = DAC_OutputBuffer_Enable;
    DAC_Init(DAC_Channel_1, &DAC_InitStructure);

    /* Enable DAC Channel1 */
    DAC_Cmd(DAC_Channel_1, ENABLE);

    /* Set DAC Channel1 DHR register:
     * DAC_OUT1 = (3.3 * 2588) / 4095 ~ 2.086 V = 3.3*(1-exp(-t/R*C)) with t=R*C */
    DAC_SetChannel1Data(DAC_Align_12b_R, 2588);
#endif /* USE_DAC */

    /* COMP Peripheral clock enable */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /* COMP4 config */

    COMP_InitStructure.COMP_NonInvertingInput = COMP4_INPUT;

#ifdef USE_DAC
    COMP_InitStructure.COMP_InvertingInput = COMP_InvertingInput_DAC1;
#elif USE_VREFINT
    COMP_InitStructure.COMP_InvertingInput = COMP_InvertingInput_VREFINT;
#endif /* USE_DAC */

    COMP_InitStructure.COMP_Output = COMP_Output_TIM4IC2;
    COMP_InitStructure.COMP_OutputPol = COMP_OutputPol_NonInverted;
    COMP_InitStructure.COMP_BlankingSrce = COMP_BlankingSrce_None;
    COMP_InitStructure.COMP_Hysteresis = COMP_Hysteresis_No;
    COMP_InitStructure.COMP_Mode = COMP_Mode_HighSpeed;
    COMP_Init(COMP_Selection_COMP4, &COMP_InitStructure);

    /* Enable COMP4 */
    COMP_Cmd(COMP_Selection_COMP4, ENABLE);
}

/**
 * @brief  Configures TIM4: channels in Input Capture mode
 * @param  None
 * @retval None
 */
static void TIM4_IC_Config(void)
{
    TIM_ICInitTypeDef TIM_ICInitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

    /* TIM4 clocks enable */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

    /* Enable the TIM global Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* Time Base configuration */
    TIM_TimeBaseStructure.TIM_Prescaler = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseStructure.TIM_Period = PeriodValue;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);

    /* TIM4 configuration: Input Capture mode ---------------------
       The external signal is connected to TIM4 CH2 pin
       The Rising edge is used as active edge,
       The TIM4 CCR2 is used to compute the frequency value
       ------------------------------------------------------------ */
    TIM_ICInitStructure.TIM_Channel     = TIM_Channel_2;
    TIM_ICInitStructure.TIM_ICPolarity  = TIM_ICPolarity_Rising;
    TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
    TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
    TIM_ICInitStructure.TIM_ICFilter = 0;
    TIM_ICInit(TIM4, &TIM_ICInitStructure);

    /* Enable the CC2 Interrupt Request */
    TIM_ITConfig(TIM4, TIM_IT_CC2, ENABLE);

    /* Slave Mode selection: TIM4 is synchronized to start with TIM3 */
    TIM_SelectSlaveMode(TIM4, TIM_SlaveMode_Trigger);
    TIM_SelectInputTrigger(TIM4, TIM_TS_ITR2);
}

/**
 * @brief  Enter Calibration menu to correct ICError and Capacitance55RH values
 * @param  None
 * @retval None
 */


/**
 * @brief  Calculate and Dispaly the humidity value on LCD
 * @param  DisplayValue : Relative Humidity in percent
 * @retval None
 */


/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
