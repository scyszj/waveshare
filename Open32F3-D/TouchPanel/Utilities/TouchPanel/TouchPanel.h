/*********************************************************************************************************
*
* File                : TouchPanel.h
* Hardware Environment:
* Build Environment   : RealView MDK-ARM  Version: 4.20
* Version             : V1.0
* By                  :
*
*                                  (c) Copyright 2005-2011, WaveShare
*                                       http://www.waveshare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

#ifndef _TOUCHPANEL_H_
#define _TOUCHPANEL_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f30x.h"

#define Open_TP_CS_PIN                  GPIO_Pin_13
#define Open_TP_CS_PORT                 GPIOC
#define Open_TP_CS_CLK                  RCC_AHBPeriph_GPIOC

#define Open_TP_IRQ_PIN                 GPIO_Pin_1
#define Open_TP_IRQ_PORT                GPIOB
#define Open_TP_IRQ_CLK                 RCC_AHBPeriph_GPIOB


#define TP_CS(x)    x ? GPIO_SetBits(Open_TP_CS_PORT, Open_TP_CS_PIN) : GPIO_ResetBits(Open_TP_CS_PORT, Open_TP_CS_PIN)

#define TP_INT_IN   GPIO_ReadInputDataBit(Open_TP_IRQ_PORT, Open_TP_IRQ_PIN)

#define Open_RCC_SPI                    RCC_APB2Periph_SPI1
#define Open_GPIO_AF_SPI                GPIO_AF_5

#define Open_SPI                        SPI1
#define Open_SPI_CLK_INIT               RCC_APB2PeriphClockCmd
#define Open_SPI_IRQn                   SPI1_IRQn
#define Open_SPI_IRQHANDLER             SPI1_IRQHandler

/* Private typedef -----------------------------------------------------------*/
typedef struct POINT
{
    uint16_t x;
    uint16_t y;
}Coordinate;


typedef struct Matrix
{
    long double An,
                Bn,
                Cn,
                Dn,
                En,
                Fn,
                Divider;
} Matrix;

/* Private variables ---------------------------------------------------------*/
extern Coordinate ScreenSample[3];
extern Coordinate DisplaySample[3];
extern Matrix matrix;
extern Coordinate display;

/* Private define ------------------------------------------------------------*/

#define CHX     0x90
#define CHY     0xd0


/* Private function prototypes -----------------------------------------------*/
void TP_Init(void);
Coordinate *Read_Ads7846(void);
void TouchPanel_Calibrate(void);
void DrawCross(uint16_t Xpos, uint16_t Ypos);
void TP_DrawPoint(uint16_t Xpos, uint16_t Ypos);
FunctionalState setCalibrationMatrix( Coordinate * displayPtr, Coordinate * screenPtr, Matrix * matrixPtr);
FunctionalState getDisplayPoint(Coordinate * displayPtr, Coordinate * screenPtr, Matrix * matrixPtr );

#endif

/*********************************************************************************************************
      END FILE
*********************************************************************************************************/


