#include "stm32f0xx.h"
#include <stdio.h>
#include "usart.h"
#include "OneWire.h"
#include "18B20.h"
RCC_ClocksTypeDef RCC_Clocks;
static volatile uint32_t TimingDelay;
void Delay(uint32_t nTime);

int main(void)
{
    uint8_t i;

//	RTC_Configuration();
    /* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500�s */
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 2000);

    Onewire_Enable_GPIO_Port();
    USART_Configuration();
    printf("\n******************************\n\r");
/**----------------------Read Temp------------------------*/

    while (1)
    {
        i = readTemp();
        printf("\n\rTemperture:%d��C\n\r", i);
        Delay(2000);
    }

}
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}


