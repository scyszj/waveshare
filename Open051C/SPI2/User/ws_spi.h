/*********************************************************************************************************
*
* File                : ws_spi.h
* Hardware Environment:
* Build Environment   : RealView MDK-ARM  Version: 4.20
* Version             : V1.0
* By                  :
*
*                                  (c) Copyright 2005-2011, WaveShare
*                                       http://www.waveshare.net
*                                          All Rights Reserved
*
*********************************************************************************************************/

#ifndef __SPI_H
#define __SPI_H

#include "stm32f0xx.h"

static void SPI_Rcc_Configuration(void);
static void GPIO_Configuration(void);
void SPI_Configuration(void);
void write_SPI(uint8_t data);
uint8_t read_SPI(void);

#endif    /*__SPI_H*/
