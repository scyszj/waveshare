#include "stm32f0xx.h"
#include <stdio.h>
#include "usart.h"

RCC_ClocksTypeDef RCC_Clocks;
static volatile uint32_t TimingDelay;
void Delay(uint32_t nTime);
void  Init_GPIOs(void);
int main(void)
{
    uint8_t Key;

    /* Configure SysTick IRQ and SysTick Timer to generate interrupts every 500�s */
    RCC_GetClocksFreq(&RCC_Clocks);
    SysTick_Config(RCC_Clocks.HCLK_Frequency / 100);

    Init_GPIOs();
    GPIO_SetBits(GPIOC, GPIO_Pin_8);    GPIO_ResetBits(GPIOC, GPIO_Pin_9);
//	USART_Configuration();
    while (1)
    {
        if (!(GPIOA->IDR & 0x0002))
        {
            Key = 1;
        }
        else if (!(GPIOA->IDR & 0x0004))
        {
            Key = 2;
        }
        else if (!(GPIOA->IDR & 0x0008))
        {
            Key = 3;
        }
        else if (!(GPIOC->IDR & 0x0010))
        {
            Key = 4;
        }
        else if (!(GPIOC->IDR & 0x0020))
        {
            Key = 5;
        }
        switch (Key)
        {
        case 1: GPIO_SetBits(GPIOC, GPIO_Pin_8); GPIO_ResetBits(GPIOC, GPIO_Pin_9);   Delay(50); break;
        case 2: GPIO_SetBits(GPIOC, GPIO_Pin_9); GPIO_ResetBits(GPIOC, GPIO_Pin_8);   Delay(50); break;
        case 3: GPIO_ResetBits(GPIOC, GPIO_Pin_8); GPIO_ResetBits(GPIOC, GPIO_Pin_9); Delay(50); GPIO_SetBits(GPIOC, GPIO_Pin_8); GPIO_SetBits(GPIOC, GPIO_Pin_9); Delay(50); break;
        case 4: GPIO_SetBits(GPIOC, GPIO_Pin_8); GPIO_SetBits(GPIOC, GPIO_Pin_9); Delay(50); break;
        case 5: GPIO_SetBits(GPIOC, GPIO_Pin_8); Delay(50);
            GPIO_SetBits(GPIOC, GPIO_Pin_9); Delay(50);
            GPIO_ResetBits(GPIOC, GPIO_Pin_9);   Delay(50);
            GPIO_ResetBits(GPIOC, GPIO_Pin_8);
            Delay(50);; break;

        }

    }

}

void  Init_GPIOs(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure the LED_pin as output push-pull for LD3 & LD4 usage*/
    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOC, ENABLE);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOA, ENABLE);
    RCC_AHBPeriphClockCmd( RCC_AHBPeriph_GPIOC, ENABLE);
    /* Key */

    GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin =   GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(GPIOC, &GPIO_InitStructure);

}
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0)
    {
        ;
    }

}

/**
 * @brief  Decrements the TimingDelay variable.
 * @param  None
 * @retval None
 */
void TimingDelay_Decrement(void)
{

    if (TimingDelay != 0x00)
    {
        TimingDelay--;
    }

}


