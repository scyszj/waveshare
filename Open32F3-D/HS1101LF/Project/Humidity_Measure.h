/**
 ******************************************************************************
 * @file    Humidity_Measure.h
 * @author  MCD Application Team
 * @version V1.0.0
 * @date    13-November-2012
 * @brief   Header for main.c module
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
 *
 * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *        http://www.st.com/software_license_agreement_liberty_v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f30x.h"
#include "stm32303c_eval.h"
#include <math.h>
#include <stdio.h>


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/* Default ICapture Offset Values for 72Mhz clock */
#define DEFAULT_OFFSET_DAC 182        //~ 55.5pF Offset
#define DEFAULT_OFFSET_VRI 153        //~ 30.6pF Offset

#define VREFINT_FACTOR log(3.26f / (3.26f - 1.22f))      /* ln(Vdd/(Vdd-VrefInt)) */

/* Polynomial Expression Coefficients: Capacitance=f(Humidity) */
#define P3 0.00000003903f
#define P2 -0.000008294f
#define P1 0.002188f
#define P0 0.898f

/* Reversed Polynomial Expression Coefficients: Humidity=f(Capacitance) */
#define RP3 -3465.6f
#define RP2 10732
#define RP1 -10457
#define RP0 3245.9f

#define REFCAP 0.00000000018f                  /* Reference Capacitance Value */
#define RES 82500                                         /* Resistance Value */
#define FREQ 1500                                 /* TIM3 PWM frequency value */
/* Min = SystemCoreClock/65535 -- Max = 1/(RES*CAP) */
#define DUTY 50                             /* TIM3 PWM duty cycle in percent */

#define USE_DAC 1                   /* DAC1 = 2.085V Reference Voltage Used */
//#define USE_VREFINT 1            /*  VrefInt = 1.22V Reference Voltage Used */

/* TIM3_PWM Output Pin Select*/
//#define TIM3_PWM_PB0 1
#define TIM3_PWM_PC8 1
//#define TIM3_PWM_PE4 1
/* Comp4 Non Inverting Input is PB0 */
#define  COMP4_INPUT COMP_NonInvertingInput_IO1

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
